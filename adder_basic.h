#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <omp.h>
#include "red_black_tree.h"

//#define numGenes 23000
//#define numDatasets 800
#define MAX_CHAR_PER_LINE 128

int numGenes;
int numDatasets;
int numQueries;

int TMP1;
int TMP2;

struct rndgenerator{
	int current;
	float *r;
	int num;
};

struct goldstd {
	int num_gold_std;
	char **gold;
	char **term_name;
	int *size;
};

struct eval_result{
	int *pr;
	int *ret_genes;
	int rel_retrieved;
	float rbp;
};

struct message_el {
	char *m;
	int m_size;
	struct message_el *next;
};

typedef struct message_el message;

struct list_el {
	float val;
	unsigned short dset;
	struct list_el * next;
};

typedef struct list_el item;

struct aresult {
	float f;
	int i;
};

struct thread_data {
	char **presence;
	unsigned char ***r;
	struct goldstd *gold;
	char *dset;
	int new_fd;
	int threadid;	
};

void CharArrayDest(void *a);
int CharArrayComp(const void *a, const void *b);
void CharArrayPrint(const void* a);

//for red black tree
void IntDest(void* a);
int IntComp(const void * a, const void* b);
void IntPrint(const void* a);

void InfoPrint(void *a);
void InfoDest(void *a);

void DoNotDestroyRBNode(void *a);
//end

//int checkInTree(rb_red_blk_tree* tree, int g1, int g2);
int aresult_cmp(const void *a, const void *b);

message* addMessage(char *m, int m_size, message *list, char *size, int limit, int isString);
message* destructMessages(message *i, char *size);

item* addItem(item *i, item *list, char *size, int limit);
item* destructList(item *i, char* size);
float sumList(item *i);
void printList(item *i);
int containsItem(item *i, unsigned short dset);
