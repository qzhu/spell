#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "adder_basic.h"
#endif


rb_red_blk_tree* readGeneMap(char *gene_map_file);
int get_gene_id(char *name, rb_red_blk_tree* gene_tree);
struct rndgenerator * read_random_numbers(char *file);
float get_random_number(struct rndgenerator *r);
char** presence_read();
void free_goldstd(struct goldstd *g);
int find_gold_std_id(char *n, struct goldstd *g);
struct goldstd* gold_standard_read(char *file);
char* dset_info_read();
int** query_read(char *term, int query_size);
float** pagerank_read(char *file, float **r);
//unsigned char*** result_read_binary(char *term);
unsigned char*** result_read_binary(FILE *infile, unsigned char ***conv);
unsigned char*** result_read_binary_normal(char *term, unsigned char ***r);
unsigned char*** result_read_binary_stdin(unsigned char ***r);
//unsigned char*** result_read(char *term);
unsigned char*** free_r(unsigned char*** c);
unsigned char*** result_read();
unsigned char*** result_read_binary_combine(char *file, unsigned char ***conv);
unsigned char*** query_result_read_binary(FILE *infile, unsigned char ***conv);
unsigned char*** result_read_binary_by_g_cust(FILE *infile, unsigned char ***conv_g, int dset_incr);
unsigned char*** result_read_binary_by_g(FILE *infile, unsigned char ***conv_g);
unsigned char*** fix_conv_g(unsigned char*** conv_g);
float** result_read_func_network(FILE *infile, float** conv_g, int gene);
float** fix_func_network_g(float ** f);
float ** read_weight(FILE *infile, float** f, int dset);
float** read_dataset_linkage_graph(char *file);
