#include "adder_file_read.h"

#ifndef COMMON
#define COMMON
#include "common.h"
#endif

void order_statistics_integrate(int l, float **res_cross, int *qq, struct Map *map_D, int query_size);
