#include "cross_validated.h"

/* create partitions for doing cross validations using query genes
 * if p is LEAVE_ONE_OUT or LEAVE_ONE_IN, fold and fold_size is ignored
 * if p is CUSTOM_PART, then generate n partitions each of size "fold_size"
 * LEAVE_ONE_IN and CUSTOM_PART has no overlapping partitions, LEAVE_ONE_OUT has
 */
int** partition_query(int *fold, int *fold_size, int *q, int query_size, gsl_rng *r, enum PartitionMode p){
	int i, j, k;
	int **cr_q;
	if(p==LEAVE_ONE_IN){
		*fold = query_size;
		*fold_size = 1;
	}else if(p==LEAVE_ONE_OUT){
		*fold = query_size;
		*fold_size = query_size - 1;
	}else if(p==CUSTOM_PART){
	}else{
		fprintf(stderr, "Invalid mode\n");
		exit(1);
	}

	cr_q = (int**)malloc((*fold)*sizeof(int*));
	cr_q[0] = (int*)malloc((*fold)*(*fold_size)*sizeof(int));
	for(i=1; i<*fold; i++){
		cr_q[i] = cr_q[i-1] + (*fold_size);
	}
	for(i=0; i<*fold; i++){
		for(j=0; j<*fold_size; j++){
			cr_q[i][j] = -1;
		}
	}

	int *q_b = (int*)malloc(query_size*sizeof(int));
	for(i=0; i<query_size; i++){
		q_b[i] = q[i];
	}

	gsl_ran_shuffle(r, q_b, query_size, sizeof(int));

	if(p==LEAVE_ONE_IN || p==CUSTOM_PART){
		k = 0;
		for(i=0; i<*fold; i++){
			for(j=0; j<*fold_size; j++){
				if(k==query_size) continue;
				cr_q[i][j] = q_b[k];
				k++;
			}
		}
	}else if(p==LEAVE_ONE_OUT){
		int current_index = -1;
		for(i=0; i<*fold; i++){
			for(j=0; j<*fold_size; j++){
				current_index = (i + j) % query_size;
				cr_q[i][j] = q_b[current_index];
			}
		}
	}

	free(q_b);
	return cr_q;
}

//return weight of each cross validation
float* weight_one_dataset_cross_validated(float **rank_cross,
	const int *q, const int query_size, const struct Presence *absent_Q, 
	const struct Presence *absent_G,
	float **f, const struct Map *map_G, const struct Map *map_Q,
	struct Presence *is_query_cross, struct Presence *is_gold,
	int fold, int **cr_q, int fold_size, const float rate){

	int i,j;
	int qj = 0;
	int qi = 0;

	#ifdef DEBUG
	if(is_query_cross==NULL || is_gold==NULL){
		fprintf(stderr, "is_query_cross null in weight_one_dataset_cross_validated\n");
		exit(1);
	}
	#endif

	float *w = (float*)malloc(fold*sizeof(float));

	for(qi=0; qi<fold; qi++){
		int num_q = 0;
		int num_v = 0;
		int this_q;
		//setting up query
		for(i=0; i<fold_size; i++){
			this_q = cr_q[qi][i];
			if(this_q==-1) continue;
			if(check_bit(this_q, absent_Q)==1) continue;
			set_bit(this_q, is_query_cross);
			num_q++;
		}

		//setting up gold standard
		for(qj=0; qj<fold; qj++){
			if(qi==qj) continue;
			for(i=0; i<fold_size; i++){
				this_q = cr_q[qj][i];
				if(this_q==-1) continue;
				if(check_bit(this_q, absent_Q)==1) continue;
				if(check_bit(this_q, is_query_cross)==1) continue;
				set_bit(this_q, is_gold);
				num_v++;
			}
		}

		if(num_q==0 || num_v==0){
			w[qi] = 0;
			continue;
		}

		float *rank = seq(f, absent_G, is_query_cross, map_Q, map_G);
		memcpy(rank_cross[qi], rank, sizeof(float)*numGenes);
		struct eval_result *er = evaluate(rank, is_query_cross, is_gold, map_G, rate);
		w[qi] = er->rbp;

		free(er->ret_genes);
		free(er->pr);
		free(er);

		//RESTORE
		for(qj=0; qj<fold; qj++){
			if(qi==qj) continue;
			for(i=0; i<fold_size; i++){
				this_q = cr_q[qj][i];
				if(this_q==-1) continue;
				if(check_bit(this_q, absent_Q)==1) continue;
				if(check_bit(this_q, is_query_cross)==1) continue;
				clear_bit(this_q, is_gold);
			}
		}
		for(i=0; i<fold_size; i++){
			this_q = cr_q[qi][i];
			if(this_q==-1) continue;
			if(check_bit(this_q, absent_Q)==1) continue;
			clear_bit(this_q, is_query_cross);
		}

		free(rank);
	}
	return w;
} 


void metacor_sing_one_dataset(
	const int *q, const int query_size, const struct Presence *absent_Q, 
	const struct Presence *absent_G,
	float **f, const struct Map *map_G, const struct Map *map_Q,
	struct Presence *is_query_cross, struct Presence *is_gold, float **res_sing_l){

	int i,j;
	int qj = 0;
	int qi = 0;

	#ifdef DEBUG
	if(is_query_cross==NULL || is_gold==NULL){
		fprintf(stderr, "is_query_cross null in metacor_sing_one_dataset\n");
		exit(1);
	}
	#endif

	for(qi=0; qi<query_size; qi++){
		int this_q = q[qi];
		if(check_bit(this_q, absent_Q)==1) continue;
		set_bit(this_q, is_gold);

		for(i=0; i<query_size; i++){
			if(check_bit(q[i], absent_Q)==1) continue;
			if(check_bit(q[i], is_gold)==1) continue;
			set_bit(q[i], is_query_cross);
		}

		float *zlist = seq(f, absent_G, is_query_cross, map_Q, map_G);
		for(i=0; i<numGenes; i++){
			res_sing_l[qi][i] += zlist[i];
		}

		//RESTORE
		clear_bit(this_q, is_gold);
		for(i=0; i<query_size; i++){
			if(check_bit(q[i], absent_Q)==1) continue;
			clear_bit(q[i], is_query_cross);
		}
		free(zlist);
	}
} 

