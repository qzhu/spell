#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif

#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

#ifndef GENE_ADDER
#define GENE_ADDER
#include "../gene_adder/sequential.h"
#endif

#ifndef EVALUATOR
#define EVALUATOR
#include "../evaluator/evaluator.h"
#endif

enum PartitionMode { LEAVE_ONE_OUT, LEAVE_ONE_IN, CUSTOM_PART };

int** partition_query(int *fold, int *fold_size, int *q, int query_size, gsl_rng *r, enum PartitionMode p);

float* weight_one_dataset_cross_validated(float **rank_cross,
	const int *q, const int query_size, const struct Presence *absent_Q, 
	const struct Presence *absent_G,
	float **f, const struct Map *map_G, const struct Map *map_Q,
	struct Presence *is_query_cross, struct Presence *is_gold,
	int fold, int **cr_q, int fold_size, const float rate);

void metacor_sing_one_dataset(
	const int *q, const int query_size, const struct Presence *absent_Q, 
	const struct Presence *absent_G,
	float **f, const struct Map *map_G, const struct Map *map_Q,
	struct Presence *is_query_cross, struct Presence *is_gold, float **res_sing_l);
