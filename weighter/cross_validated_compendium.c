
//does not work for macro: EQUAL_WEIGHTED && NO_SUBAVGZ, METACOR_SING
float *** repeat_query(unsigned char ***r, 
	int **q, int numQueries, 
	int numDsetLoad, 
	int *map_dr, 
	struct Presence **present_g,
	int query_size)
{

	int numGenesD;
	int d, dd;
	int j, jj;
	int i, ii, l;
	int qi;
	char *q_to_load = (char*)malloc(numGenes);   //numGenes assumed
	unsigned char *map_q = (unsigned char*)malloc(numGenes);  //assume unique query genes do not exceed 0 254
	int *map_qr = (int*)malloc(numGenes*sizeof(int));
	int num_unique_q = 0;

	for(i=0; i<numGenes; i++){
		q_to_load[i] = 0;
		map_q[i] = 255;
		map_qr[i] = -1;
	}

	j = 0;
	for(l=0; l<numQueries; l++){
		for(i=0; i<query_size; i++){
			q_to_load[q[l][i]]=1;
			if(map_q[q[l][i]]==255){
				map_q[q[l][i]] = j;
				map_qr[j] = q[l][i];
				j++;
			}
		}
	}

	for(i=0; i<numGenes; i++){
		if(map_q[i]!=255){
			num_unique_q++;
		}
	}
	//printf("NumQuerySize: %d\n", j);

	float ***res_cross = (float***)malloc(numQueries*sizeof(float**));
	res_cross[0] = (float**)malloc(numQueries*numDsetLoad*sizeof(float*));
	for(l=1; l<numQueries; l++){
		res_cross[l] = res_cross[l-1] + numDsetLoad; 
	}
	res_cross[0][0] = (float*)malloc(numQueries*numDsetLoad*numGenes*sizeof(float));
	for(l=0; l<numQueries; l++){
		for(i=0; i<numDsetLoad; i++){
			if(i==0 && l>0){
				res_cross[l][i] = res_cross[l-1][numDsetLoad-1] + numGenes;
			}else if(i>=1){
				res_cross[l][i] = res_cross[l][i-1] + numGenes;
			}
		}
	}

	for(dd=0; dd<numDsetLoad; dd++){
		d = map_dr[dd];
		char *absent_g = (char*)malloc(numGenes);
		char *absent_q = (char*)malloc(numGenes);

		int *map_gr = (int*)malloc(numGenes*sizeof(int));
		for(i=0; i<numGenes; i++){
			map_gr[i] = -1;
		}

		for(jj=0; jj<num_unique_q; jj++){
			j = map_qr[jj];
			if(check_bit(j, present_g[dd])==0){
				absent_q[j] = 1;
			}else{
				absent_q[j] = 0;
			}
		}

		for(i=0; i<numGenes; i++){
			if(map_q[i]!=255){
				absent_g[i] = 0;
				continue;
			}
			if(check_bit(i, present_g[dd])==0){
				absent_g[i] = 1;
			}else{
				absent_g[i] = 0;
			}
		}

		j=0;
		for(i=0; i<numGenes; i++){
			if(absent_g[i]==1) continue;
			map_gr[j] = i;
			j++;
		}
		numGenesD = j;
		//printf("NumGenesD %d\n", j); getchar();

		float **fi = (float**)malloc(numGenesD*sizeof(float*));
		fi[0] = (float*)malloc(numGenes*num_unique_q*sizeof(float));
		for(i=1; i<numGenesD; i++){
			fi[i] = fi[i-1] + num_unique_q;
		}
		for(i=0; i<numGenesD; i++){
			for(j=0; j<num_unique_q; j++){
				fi[i][j] = 0;
			}
		}

		float** f = fi;
		for(ii=0; ii<numGenesD; ii++){
			i = map_gr[ii];
	
			for(jj=0; jj<num_unique_q; jj++){
				j = map_qr[jj];

				if(i==j){
					f[ii][jj] = 0;
				}
				if(r[d][j]==NULL) continue;

				//printf("%d\n", (int) r[d][j][i]); 
				if(r[d][j][i] != 255){
					float rf = (float) r[d][j][i];
					f[ii][jj] = rf;
				}

			}
		}

		char *is_query = (char*)malloc(numGenes);
		char *is_gold = (char*)malloc(numGenes);

		for(i=0; i<numGenes; i++){
			is_query[i] = 0;
			is_gold[i] = 0;
		}

		for(l=0; l<numQueries; l++){
			int q_size = 0;
			//SET
			for(qi=0; qi<query_size; qi++){
				if(absent_q[q[l][qi]]==1) continue;
				is_query[q[l][qi]] = 1;
				q_size++;
			}
			float *rank_normal = seq(f, absent_g, is_query, map_qr, num_unique_q, map_gr, numGenesD);
			//RESTORE
			for(qi=0; qi<query_size; qi++){
				if(absent_q[q[l][qi]]==1) continue;
				is_query[q[l][qi]] = 0;
			}
			for(i=0; i<numGenes; i++){
				res_cross[l][dd][i] = rank_normal[i];
				/*if(rank_normal[i]!=0){
					printf("%.2f ", res_cross[l][dd][i]);
				}*/
			}
			//printf("\n");
			free(rank_normal);
		}
		
		free(fi[0]);
		free(fi);
		free(is_query);
		free(is_gold);
		free(absent_g);
		free(absent_q);
		free(map_gr);
	}
	free(map_q);
	free(map_qr);
	free(q_to_load);

	return res_cross;

}

void output_results_reintegrate_top(float **w, float ***res_cross, int **q, int numQueries,
	int numDsetLoad, int *map_dr, int query_size)
{
	int i, j, k, l;
	int jj;
	struct aresult *weights = 
		(struct aresult*)malloc(numDsetLoad*sizeof(struct aresult));
	struct aresult *b = (struct aresult*)malloc(numGenes*sizeof(struct aresult));
	char *is_query = (char*)malloc(numGenes);
	char *is_gold = (char*)malloc(numGenes);
	for(i=0; i<numGenes; i++){
		is_query[i] = 0;
		is_gold[i] = 0;
	}

	float TOP = 0.05;
	int cut_point = (float) numDsetLoad * TOP;
	for(l=0; l<numQueries; l++){
		printf("Query %d\n", l);

		for(i=0; i<numDsetLoad; i++){
			weights[i].i = i;
			weights[i].f = w[l][map_dr[i]];
		}
		qsort(weights, numDsetLoad, sizeof(struct aresult), aresult_cmp);
		float *fr = (float*)malloc(numGenes*sizeof(float));
		for(i=0; i<query_size; i++){
			is_query[q[l][i]] = 1;
		}
		for(j=0; j<numGenes; j++){
			fr[j] = 0;
		}
		float total_weight = 0;
		for(j=0; j<numDsetLoad; j++){
			if(j>=cut_point) break;
			if(weights[j].f<=0.00001) break;
			for(k=0; k<numGenes; k++){
				int dset = weights[j].i;
				fr[k] += weights[j].f * res_cross[l][dset][k];
			}
			total_weight+=weights[j].f;
		}

		for(j=0; j<numGenes; j++){
			b[j].i = j;
			b[j].f = fr[j];
		}
		qsort(b, numGenes, sizeof(struct aresult), aresult_cmp);

		printf("Weights:\n");
		for(i=0; i<cut_point; i++){
			printf("%d %.2f%%\n", map_dr[weights[i].i], weights[i].f / total_weight * 100.0);
		}

		printf("Results:\n");
		for(i=0, jj=0; jj<500; i++){
			if(is_query[b[i].i]==1){
				continue;
			}
			printf("%d %.5f\n", b[i].i, b[i].f);
			jj++;
		}

		for(i=0; i<query_size; i++){
			is_query[q[l][i]] = 0;
		}
		free(fr);
	}

	free(weights);
	free(res_cross[0][0]);
	free(res_cross[0]);
	free(res_cross);
	free(b);
	free(is_query);
	free(is_gold);
}


void output_results_cross(
	float **w, float ***res_cross, int **q, int numQueries, 
	unsigned char ***r, int numDsetLoad, int *map_dr, 
	struct Presence **present_g, int query_size
){

	int i, d, l, j;
	int jj;
	int k;
	int qi;
	int **new_q = (int**)malloc(numQueries*sizeof(int*));
	new_q[0] = (int*)malloc(numQueries*1*sizeof(int));
	for(i=1; i<numQueries; i++){
		new_q[i] = new_q[i-1] + 1;
	}

	struct aresult *weights = (struct aresult*)malloc(numDsetLoad*sizeof(struct aresult));

	int *par_up = (int*)malloc(10*sizeof(int));
	float *par = (float*)malloc(10*sizeof(float));
	par[0] = 0.5;
	par[1] = 0.6;
	par[2] = 0.7;
	par[3] = 0.8;
	par[4] = 0.9; par[5] = 0.92; par[6] = 0.94; par[7] = 0.96; par[8] = 0.98; par[9] = 0.99;

	float **tot_par = (float**)malloc(numQueries*sizeof(float*));
	tot_par[0] = (float*)malloc(numQueries*10*sizeof(float));
	for(i=1; i<numQueries; i++){
		tot_par[i] = tot_par[i-1] + 10;
	}
	for(i=0; i<numQueries; i++){
		for(j=0; j<10; j++){
			tot_par[i][j] = 0;
		}
	}

	for(i=0; i<10; i++){
		float x= 0;
		for(j=0; j<numGenes; j++){
			x+=(1.0-par[i])*pow(par[i], j);
			if(x>=0.999) break;
		}
		par_up[i] = j;
	}

	char *is_query = (char*)malloc(numGenes);
	char *is_gold = (char*)malloc(numGenes);
	for(i=0; i<numGenes; i++){
		is_query[i] = 0;
		is_gold[i] = 0;
	}
	struct aresult *b = (struct aresult*)malloc(numGenes*sizeof(struct aresult));

	for(qi=0; qi<query_size; qi++){
		for(l=0; l<numQueries; l++){
			new_q[l][0] = q[l][qi];
		}
		float ***one_run = repeat_query(r, new_q, numQueries, numDsetLoad, map_dr, present_g, 1);
		//printf("CROSS %d\n", qi);
		for(l=0; l<numQueries; l++){
			for(i=0; i<numDsetLoad; i++){
				weights[i].i = i;
				weights[i].f = w[l][map_dr[i]];
			}
			qsort(weights, numDsetLoad, sizeof(struct aresult), aresult_cmp);
			/*for(j=0; j<numDsetLoad; j++){
				printf("W%d %.5f\n", weights[j].i, weights[j].f);
			}
			getchar();*/
			float *fr = (float*)malloc(numGenes*sizeof(float));

			is_query[new_q[l][0]] = 1;
			for(i=0; i<query_size; i++){
				if(q[l][i]==new_q[l][0]) continue;
				is_gold[q[l][i]] = 1;
			}

			for(i=0; i<10; i++){
				for(j=0; j<numGenes; j++){
					fr[j] = 0;
				}
				for(j=0; j<numDsetLoad; j++){
					if(j>=par_up[i]) break;
					if(weights[j].f<=0.00001) break;
					for(k=0; k<numGenes; k++){
						int dset = weights[j].i;
						fr[k] += (1.0 - par[i]) * pow(par[i], j) * one_run[l][dset][k];
					}
				}
				for(j=0; j<numGenes; j++){
					b[j].i = j;
					b[j].f = fr[j];
				}
				qsort(b, numGenes, sizeof(struct aresult), aresult_cmp);
				
				float x = 0;
				for(j=0; j<numGenes; j++){
					if(b[j].f == 0) break;
					if(j>=par_up[i]) break;
					if(is_gold[b[j].i]==1){
						x+=(1.0-0.95)*pow(0.95, j);
					}
				}
				if(i==0 && x<0.00001){
					break;
				}
				tot_par[l][i] += x;
				//printf("Query %d: par %d: %.5f\n", l, i, x);
			}

			is_query[new_q[l][0]] = 0;
			for(i=0; i<query_size; i++){
				is_gold[q[l][i]] = 0;
			}

			free(fr);
		}
		free(one_run[0][0]);
		free(one_run[0]);
		free(one_run);
	}



	float ***final_run = repeat_query(r, q, numQueries, numDsetLoad, map_dr, present_g, query_size);
	for(l=0; l<numQueries; l++){
		printf("Query %d\n", l);
		float max_par = 0;
		int max_id = -1;
		for(i=0; i<10; i++){
			if(tot_par[l][i]>=max_par){
				max_par = tot_par[l][i];	
				max_id = i;
			}
		}
		float pp = par[max_id];
		for(i=0; i<numDsetLoad; i++){
			weights[i].i = i;
			weights[i].f = w[l][map_dr[i]];
		}
		qsort(weights, numDsetLoad, sizeof(struct aresult), aresult_cmp);
		float *fr = (float*)malloc(numGenes*sizeof(float));
		for(i=0; i<query_size; i++){
			is_query[q[l][i]] = 1;
		}
		for(i=0; i<10; i++){
			for(j=0; j<numGenes; j++){
				fr[j] = 0;
			}
			for(j=0; j<numDsetLoad; j++){
				if(j>=par_up[i]) break;
				if(weights[j].f<=0.00001) break;
				for(k=0; k<numGenes; k++){
					int dset = weights[j].i;
					fr[k] += (1.0 - pp) * pow(pp, j) * final_run[l][dset][k];
				}
			}
		}

		for(j=0; j<numGenes; j++){
			b[j].i = j;
			b[j].f = fr[j];
		}
		qsort(b, numGenes, sizeof(struct aresult), aresult_cmp);

		printf("Weights:\n");
		for(i=0; i<par_up[max_id]; i++){
			printf("%d %.2f%%\n", map_dr[weights[i].i], ((1.0-pp)*pow(pp, i) / 0.999 * 100.0));
		}

		printf("Results:\n");
		for(i=0, jj=0; jj<500; i++){
			if(is_query[b[i].i]==1){
				continue;
			}
			printf("%d %.5f\n", b[i].i, b[i].f);
			jj++;
		}

		for(i=0; i<query_size; i++){
			is_query[q[l][i]] = 0;
		}
		free(fr);
	}

	free(final_run[0][0]);
	free(final_run[0]);
	free(final_run);

	free(b);
	free(is_gold);
	free(is_query);
	free(par);
	free(par_up);
	free(new_q[0]);
	free(new_q);
	free(weights);
}

