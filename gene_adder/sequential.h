#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif

#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

float * seq(
	float **f, const struct Presence* absent_G,
	const struct Presence *is_query, const struct Map *map_Q, const struct Map *map_G);
