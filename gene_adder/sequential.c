/*
 * sequential.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */
#include "sequential.h"

float * seq(
	float **f, const struct Presence* absent_G,
	const struct Presence *is_query, const struct Map *map_Q, const struct Map *map_G)
{
	int it;
	int i, j;
	int ii, jj;
	int n;

	float *rank = init_float(numGenes, 0);
	float *new_rank = init_float(numGenes, 0);

	int q_size = 0;
	int num_unique_q = map_Q->num_set;
	int numGenesD = map_G->num_set;

	for(jj=0; jj<num_unique_q; jj++){
		j = get_Map_r(jj, map_Q);
		if(check_bit(j, is_query)==1){
			q_size++;
		}
	}

	for(i=0; i<numGenes; i++){
		if(check_bit(i, is_query)==1){
			rank[i] = 1.0 / q_size;
		}
	}
	for(ii=0; ii<numGenesD; ii++){
		i = get_Map_r(ii, map_G);
		for(jj=0; jj<num_unique_q; jj++){
			j = get_Map_r(jj, map_Q);
			if(check_bit(j, is_query)==0) continue;
			if(j==i) continue;		//prevent calculating score with itself
			new_rank[i] += rank[j]*f[ii][jj];
		}
	}
	for(ii=0; ii<numGenesD; ii++){
		i = get_Map_r(ii, map_G);
		rank[i] = new_rank[i];
	}

	free(new_rank);
	return rank;

}
