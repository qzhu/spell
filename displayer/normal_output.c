/*
 * normal_ouput.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */
#include "normal_output.h"

void output_results(
	int l, float *w, float *sum_w, int *counts, float *res, int *qq,
	int query_size, struct Map *map_D,
	struct StrIntMap *global_map)
{

	int i, d, j;
	int qi;
	int ii, jj;

	struct aresult *singleResult = (struct aresult*)malloc(numGenes*sizeof(struct aresult));
	struct aresult *weights = (struct aresult*)malloc(numDatasets*sizeof(struct aresult));
	int numDatasets = map_D->size;
	int numDatasetsD = map_D->num_set;

	char *is_query = (char*)malloc(numGenes);
	for(i=0; i<numGenes; i++){
		is_query[i] = 0;
	}

	for(d=0; d<numDatasets; d++){
		weights[d].i = d;
		weights[d].f = 0;
	}

	printf("Query %d\n", l);
	float total_weight = 0;
	//float stdev_weight = 0;
	for(i=0; i<numDatasets; i++){
		weights[i].i = i;
		weights[i].f = 0;
	}

	for(ii=0; ii<numDatasetsD; ii++){
		i = get_Map_r(ii, map_D);
		weights[i].f = w[ii];
		total_weight += weights[i].f;
	}

	qsort(weights, numDatasets, sizeof(struct aresult), aresult_cmp);
	printf("Weights:\n");
	for(i=0; weights[i].f>=0.00001; i++){
		printf("%s %.2f %.2f%%\n", get_StrIntMap_str(weights[i].i, global_map),
			(weights[i].f * 100.0), (weights[i].f / total_weight * 100.0));
	}

	for(i=0; i<numGenes; i++){
		singleResult[i].i = i;
		singleResult[i].f = res[i];
	}

	qsort(singleResult, numGenes, sizeof(struct aresult), aresult_cmp);

	for(qi=0; qi<query_size; qi++){
		is_query[qq[qi]] = 1;
	}

	printf("Results:\n");
	for(i=0, jj=0; jj<500; i++){
		if(is_query[singleResult[i].i]==1){
			continue;
		}
		int a_count = counts[singleResult[i].i];
		if(a_count<(int) (0.5*numDatasetsD)){
			continue;
		}
		//printf("%d %.5f %.2f %d\n", singleResult[i].i, singleResult[i].f,
		//	get_Float2D_xy(l, singleResult[i].i, sum_w), a_count);
		printf("%d %.5f\n", singleResult[i].i, singleResult[i].f);
		jj++;
	}

	//RESTORE
	for(qi=0; qi<query_size; qi++){
		is_query[qq[qi]] = 0;
	}

	free(is_query);
	free(singleResult);
	free(weights);
}

