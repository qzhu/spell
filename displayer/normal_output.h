#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif

#ifndef GENE_PRESENCE
#define GENE_PRESENCE
#include "../gene_presence.h"
#endif

#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

void output_results(
	int l, float *w, float *sum_w, int *counts, float *res, int *qq,
	int query_size, struct Map *map_D,
	struct StrIntMap *global_map);
