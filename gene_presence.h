#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "adder_basic.h"
#endif

struct Presence{
	char *p;
	int size_char;
	int num_bits;
};

struct Presence * init_Presence_2d(int num_bits, int size);
struct Presence * init_Presence(int size);
struct Presence * init_Presence_from_char(char *c, int num_bits);
struct Presence * copy_Presence(struct Presence *p);
void free_Presence(struct Presence *c);
void free_Presence_2d(struct Presence *p, int size);
void clear_Presence(struct Presence *c);

int check_bit(int g, const struct Presence *c);
void set_bit(int g, struct Presence *c);
void clear_bit(int g, struct Presence *c);

