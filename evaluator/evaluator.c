#include "evaluator.h"

struct aresult* sort_rank_vector(const float *rank, const struct Map* map_G, int *num_non_zero){
	int numNonZero = 0;
	int ii, jj;
	int i;

	int numGenesD = map_G->num_set;
	//new========================
	float old_target = 0;
	float new_target = 0;
	float prev_target = 0;
	//===========================
	int prev_numNonZero = 0;

	while(1){
		numNonZero = 0;
		for(ii=0; ii<numGenesD; ii++){
			i = get_Map_r(ii, map_G);
			//if(rank[i]==0) continue;
			if(rank[i]<=old_target) continue;
			new_target+=rank[i];
			numNonZero++;
		}
		if(numNonZero==0 || numNonZero<1000){
			old_target = prev_target;
			numNonZero = prev_numNonZero;
			break;
		}
		new_target /= (float) numNonZero;
		if(new_target==old_target){
			break;
		}
		prev_target = old_target;
		old_target = new_target;
		prev_numNonZero = numNonZero;
	}

	/*
	numNonZero = 0;
	for(ii=0; ii<numGenesD; ii++){
		i = get_Map_r(ii, map_G);
		if(rank[i]==0) continue;
		if(rank[i]<=old_target) continue;
		numNonZero++;
	}*/

	*num_non_zero = numNonZero;

	if(numNonZero==0){
		return NULL;
	}

	struct aresult *singleResult = (struct aresult*)malloc(numNonZero*sizeof(struct aresult));
	jj = 0;
	for(ii=0; ii<numGenesD; ii++){
		i = get_Map_r(ii, map_G);
		//if(rank[i]==0) continue;
		//NEW===========================
		if(rank[i]<=old_target) continue;
		//==============================
		singleResult[jj].i = i;
		//singleResult[jj].f = rank[i]*1000.0;
		singleResult[jj].f = rank[i];
		jj++;
	}

	qsort(singleResult, numNonZero, sizeof(struct aresult), aresult_cmp);

	return singleResult;
}

struct eval_result* evaluate(
	const float *rank, const struct Presence *is_query, const struct Presence *is_gold, 
	const struct Map *map_G, const float p){

	int i, ii, j, jj;
	struct eval_result *er = (struct eval_result*)malloc(sizeof(struct eval_result));

	int *num_non_zero = (int*)malloc(sizeof(int));

	struct aresult *singleResult = sort_rank_vector(rank, map_G, num_non_zero);
	int numNonZero = *num_non_zero;
	free(num_non_zero);

	int *pr = (int*)malloc(2000*sizeof(int));
	int *ret_genes = (int*)malloc(2000*sizeof(int));

	if(singleResult==NULL){
		er->rbp = 0;
		er->pr = pr;
		er->rel_retrieved = 0;
		er->ret_genes = ret_genes;
		return er;
	}

	int rel_retrieved = 0;
	jj = 0;
	float x = 0;
	//printf("num non zero: %d\n", numNonZero);
	for(i=0; i<numNonZero; i++){
		if(singleResult[i].f<=0){
			break;
		}
		if(check_bit(singleResult[i].i, is_query)==1){
			continue;
		}
		if(check_bit(singleResult[i].i, is_gold)==1){
			x+=pow(p, jj);
			pr[rel_retrieved] = jj;
			ret_genes[rel_retrieved] = singleResult[i].i;
			rel_retrieved++;
		}
		jj++;
	}
	x*=(1.0 - p);

	/*
	float x = 0;
	for(i=0; i<rel_retrieved; i++){
		x+=pow(p, pr[i]);
	}
	x*=(1.0 - p);
	*/
	er->rbp = x;
	er->rel_retrieved = rel_retrieved;
	er->pr = pr;
	er->ret_genes = ret_genes;

	free(singleResult);
	return er;

}
