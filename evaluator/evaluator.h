#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif

#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

struct aresult* sort_rank_vector(const float *rank, const struct Map* map_G, int *num_non_zero);
struct eval_result* evaluate(
	const float *rank, const struct Presence *is_query, const struct Presence *is_gold, 
	const struct Map *map_G, const float p);
