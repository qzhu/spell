#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "adder_basic.h"
#endif

#ifndef COMMON
#define COMMON
#include "common.h"
#endif

#ifndef SINFO
#define SINFO
#include "reader/sinfo_file_reader.h"
#endif


float **prepare_gene_map(struct Presence *absent_G, struct Presence *absent_Q, struct Map *map_G,
	const struct Presence *present_g, const struct Map *map_Q);

float **prepare_one_query_in_one_dataset_metacor(
	struct Presence *absent_G, struct Presence *absent_Q, struct Map *map_G,
	const struct Presence *present_g, const struct Map *map_Q,
	unsigned char **rd, 
	const struct Float2D *avg,
	const struct sinfo *si, const int d);

float **prepare_one_query_in_one_dataset_no_subavgz(
	struct Presence *absent_G, struct Presence *absent_Q, struct Map *map_G,
	const struct Presence *present_g, const struct Map *map_Q,
	unsigned char **rd, 
	const struct Float2D *avg, const int d);

float **prepare_one_query_in_one_dataset_normal(
	struct Presence *absent_G, struct Presence *absent_Q, struct Map *map_G,
	const struct Presence *present_g, const struct Map *map_Q,
	unsigned char **rd);
