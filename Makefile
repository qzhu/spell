COMPILER = gcc
CCFLAGS = -O2 -fopenmp -g

DEP1 = stack.o misc.o red_black_tree.o adder_file_read.o adder_basic.o gene_presence.o common.o reader/var_file_reader.o \
reader/sinfo_file_reader.o reader/avg_file_reader.o weighter/cross_validated.o evaluator/evaluator.o displayer/normal_output.o \
dataset_adder/linear_combination.o gene_adder/sequential.o adder_one_query.o combination.o normalizer/rank_transform.o \
reader/settings_reader.o reader/platform_avg_reader.o reader/gene_data_reader.o reader/query_reader.o \
normalizer/subtract_platform_avg.o

adder_queries_vw: adder_pagerank_one_query.apr1.vw.o $(DEP1)
	$(COMPILER) $(CCFLAGS) adder_pagerank_one_query.apr1.vw.o $(DEP1) -o adder_queries_vw -lgsl -lgslcblas -lm

adder_queries_w: adder_pagerank_one_query.apr1.w.o $(DEP1)
	$(COMPILER) $(CCFLAGS) adder_pagerank_one_query.apr1.w.o $(DEP1) -o adder_queries_w -lgsl -lgslcblas -lm

adder_queries_eq_w: adder_pagerank_one_query.apr1.eq_w.o $(DEP1)
	$(COMPILER) $(CCFLAGS) adder_pagerank_one_query.apr1.eq_w.o $(DEP1) -o adder_queries_eq_w -lgsl -lgslcblas -lm

adder_queries_order: adder_pagerank_one_query.apr1.order.o $(DEP1)
	$(COMPILER) $(CCFLAGS) adder_pagerank_one_query.apr1.order.o $(DEP1) -o adder_queries_order -lgsl -lgslcblas -lm

adder_pagerank_one_query.apr1.vw.o: adder_pagerank_one_query.apr1.c
	$(COMPILER) -D VARIANCE_WEIGHTING $(CCFLAGS) -c adder_pagerank_one_query.apr1.c -o adder_pagerank_one_query.apr1.vw.o

adder_pagerank_one_query.apr1.w.o: adder_pagerank_one_query.apr1.c
	#$(COMPILER) -D KEEP_DSET_SCORE -D WEIGHTED $(CCFLAGS) -c adder_pagerank_one_query.apr1.c -o adder_pagerank_one_query.apr1.w.o
	$(COMPILER) -D WEIGHTED $(CCFLAGS) -c adder_pagerank_one_query.apr1.c -o adder_pagerank_one_query.apr1.w.o

adder_pagerank_one_query.apr1.eq_w.o: adder_pagerank_one_query.apr1.c
	$(COMPILER) -D EQUAL_WEIGHTED $(CCFLAGS) -c adder_pagerank_one_query.apr1.c -o adder_pagerank_one_query.apr1.eq_w.o

adder_pagerank_one_query.apr1.order.o: adder_pagerank_one_query.apr1.c
	$(COMPILER) -D ORDER_STATISTICS $(CCFLAGS) -c adder_pagerank_one_query.apr1.c -o adder_pagerank_one_query.apr1.order.o


stack.o: stack.c
	$(COMPILER) $(CCFLAGS) -c stack.c
misc.o: misc.c
	$(COMPILER) $(CCFLAGS) -c misc.c
red_black_tree.o: red_black_tree.c
	$(COMPILER) $(CCFLAGS) -c red_black_tree.c
adder_file_read.o: adder_file_read.c
	$(COMPILER) $(CCFLAGS) -c adder_file_read.c
adder_basic.o: adder_basic.c
	$(COMPILER) $(CCFLAGS) -c adder_basic.c
gene_presence.o: gene_presence_char.c
	$(COMPILER) $(CCFLAGS) -c gene_presence_char.c -o gene_presence.o
#gene_presence.o: gene_presence_bit.c
#	$(COMPILER) $(CCFLAGS) -c gene_presence_bit.c -o gene_presence.o

common.o: common.c
	$(COMPILER) $(CCFLAGS) -c common.c
reader/gene_data_reader.o: reader/gene_data_reader.c
	$(COMPILER) $(CCFLAGS) -c reader/gene_data_reader.c -o reader/gene_data_reader.o
reader/platform_avg_reader.o: reader/platform_avg_reader.c
	$(COMPILER) $(CCFLAGS) -c reader/platform_avg_reader.c -o reader/platform_avg_reader.o
reader/settings_reader.o: reader/settings_reader.c
	$(COMPILER) $(CCFLAGS) -c reader/settings_reader.c -o reader/settings_reader.o
reader/var_file_reader.o: reader/var_file_reader.c
	$(COMPILER) $(CCFLAGS) -c reader/var_file_reader.c -o reader/var_file_reader.o
reader/sinfo_file_reader.o: reader/sinfo_file_reader.c
	$(COMPILER) $(CCFLAGS) -c reader/sinfo_file_reader.c -o reader/sinfo_file_reader.o
reader/avg_file_reader.o: reader/avg_file_reader.c
	$(COMPILER) $(CCFLAGS) -c reader/avg_file_reader.c -o reader/avg_file_reader.o
reader/query_reader.o: reader/query_reader.c
	$(COMPILER) $(CCFLAGS) -c reader/query_reader.c -o reader/query_reader.o
weighter/cross_validated.o: weighter/cross_validated.c
	$(COMPILER) $(CCFLAGS) -c weighter/cross_validated.c -o weighter/cross_validated.o
evaluator/evaluator.o: evaluator/evaluator.c
	$(COMPILER) $(CCFLAGS) -c evaluator/evaluator.c -o evaluator/evaluator.o
displayer/normal_output.o: displayer/normal_output.c
	$(COMPILER) $(CCFLAGS) -c displayer/normal_output.c -o displayer/normal_output.o
dataset_adder/linear_combination.o: dataset_adder/linear_combination.c
	$(COMPILER) $(CCFLAGS) -c dataset_adder/linear_combination.c -o dataset_adder/linear_combination.o
gene_adder/sequential.o: gene_adder/sequential.c
	$(COMPILER) $(CCFLAGS) -c gene_adder/sequential.c -o gene_adder/sequential.o
adder_one_query.o: adder_one_query.c
	$(COMPILER) $(CCFLAGS) -c adder_one_query.c
combination.o: combination.c
	$(COMPILER) -Wall $(CCFLAGS) -c combination.c

#optional----------------
normalizer/normalizer.o: normalizer/normalizer.c
	$(COMPILER) $(CCFLAGS) -c normalizer/normalizer.c -o normalizer/normalizer.o
normalizer/rank_transform.o: normalizer/rank_transform.c
	$(COMPILER) $(CCFLAGS) -c normalizer/rank_transform.c -o normalizer/rank_transform.o
normalizer/subtract_platform_avg.o: normalizer/subtract_platform_avg.c
	$(COMPILER) $(CCFLAGS) -c normalizer/subtract_platform_avg.c -o normalizer/subtract_platform_avg.o

#------------------------


clean:
	rm -rf *.o
	rm -rf adder_queries_vw adder_queries_w adder_queries_order adder_queries_eq_w
	rm -rf reader/*.o weighter/*.o displayer/*.o dataset_adder/*.o gene_adder/*.o evaluator/*.o	normalizer/*.o

copy:
	if [ -d hg ]; then \
	mkdir hg/reader hg/weighter hg/displayer hg/dataset_adder hg/gene_adder hg/evaluator hg/normalizer; \
	cp reader/*.c reader/*.h hg/reader/.; \
	cp weighter/*.c weighter/*.h hg/weighter/.; \
	cp displayer/*.c displayer/*.h hg/displayer/.; \
	cp dataset_adder/*.c dataset_adder/*.h hg/dataset_adder/.; \
	cp gene_adder/*.c gene_adder/*.h hg/gene_adder/.; \
	cp evaluator/*.c evaluator/*.h hg/evaluator/.; \
	cp normalizer/*.c normalizer/*.h hg/normalizer/.; \
	cp *.c *.h hg/.; \
	cp Makefile hg/.; fi
