#include "gene_presence.h"

/*
 * gene_presence.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */

struct Presence * init_Presence_from_char(char *c, int num_bits){
	struct Presence *d = init_Presence(num_bits);
	int j = 0;
	for(j=0; j<num_bits; j++){
		if(c[j]==1){
			set_bit(j, d);
		}
	}
	return d;
}


struct Presence * init_Presence(int num_bits){
	struct Presence *c = (struct Presence *) malloc(sizeof(struct Presence));
	int i = (int) (num_bits / 8);
	if(num_bits % 8 > 0){
		i++;
	}
	c->p = (char*)malloc(i);
	c->size_char = i;
	c->num_bits = num_bits;

	int j = 0;
	for(j=0; j<c->size_char; j++){
		c->p[j] = 0;
	}

	return c;
}

void free_Presence(struct Presence *c){
	free(c->p);
	free(c);
}

void clear_Presence(struct Presence *c){
	int i;
	for(i=0; i<c->size_char; i++){
		c->p[i] = 0;
	}
}

//check_present
int check_bit(int g, const struct Presence *c){
	#ifdef DEBUG
	if(c==NULL){
		fprintf(stderr, "Error!!!\n");
		exit(1);
	}
	if(g >= c->num_bits){
		fprintf(stderr, "Index out of range, gene presence\n");
		exit(1);
	}
	#endif

	int offset = (int) (g / 8);
	int remainder = g % 8;

	unsigned char *p = c->p + offset;
	unsigned char x = 128 >> remainder;
	unsigned char r = x & (*p);
	r = r >> (7 - remainder);
	return (int) r;
}

void set_bit(int g, struct Presence *c){
	#ifdef DEBUG
	if(c==NULL){
		fprintf(stderr, "Error!!!\n");
		exit(1);
	}
	if(g >= c->num_bits){
		fprintf(stderr, "Index out of range, gene presence\n");
		exit(1);
	}
	#endif

	int offset = (int) (g / 8);
	int remainder = g % 8;
	unsigned char *p = c->p + offset;
	unsigned char x = 128 >> remainder;
	//printf("s%d\n", x);
	*p = x | (*p);
}

void clear_bit(int g, struct Presence *c){
	#ifdef DEBUG
	if(c==NULL){
		fprintf(stderr, "Error!!!\n");
		exit(1);
	}
	if(g >= c->num_bits){
		fprintf(stderr, "Index out of range, gene presence\n");
		exit(1);
	}
	#endif
	int offset = (int) (g / 8);
	int remainder = g % 8;
	unsigned char *p = c->p + offset;
	unsigned char x = 128 >> remainder;
	x = ~x;
	*p = (*p) & x;
}
