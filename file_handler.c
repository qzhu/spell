#include "file_handler.h"
#include <assert.h>

unsigned short* read_freq_map(){
	FILE *infile;
	char *line, *ret;
	int len;
	int i;
	
	int lineLen = MAX_CHAR_PER_LINE;
	
	unsigned short *map = (unsigned short*)malloc(numGenes*sizeof(unsigned short));
	for(i=0; i<numGenes; i++){
		map[i] = 0;
	}

	if((infile= fopen("freq_map.txt", "r")) == NULL){
		fprintf(stderr, "Error no such file \n");
		return NULL;
	}
	line = (char*) malloc(lineLen);
	while(fgets(line, lineLen, infile)!=NULL){
		while(strlen(line)==lineLen -1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}
	}
	rewind(infile);
	
	while(fgets(line, lineLen, infile)!=NULL){
		char *p = strtok(line, " \t\n");
		char *p2 = strtok(NULL, " \t\n");
		map[atoi(p)] = (unsigned short) atoi(p2);
	}
	fclose(infile);
	return map;
}

char* gold_standard_read(char *_term){
	FILE *infile;
	char *line, *ret;
	int len;
	int i;
	
	int lineLen = MAX_CHAR_PER_LINE;
	char term[50];
	//sprintf(term, "QZ_%s", _term);
	sprintf(term, "%s", _term);
	
	char *gold = (char*)malloc(numGenes*sizeof(char));
	for(i=0; i<numGenes; i++){
		gold[i] = 0;
	}
	if((infile= fopen("all_non_overlap_terms_mapped.nov9", "r")) == NULL){
		fprintf(stderr, "Error no such file \n");
		return NULL;
	}
	line = (char*) malloc(lineLen);
	while(fgets(line, lineLen, infile)!=NULL){
		while(strlen(line)==lineLen -1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}
	}
	rewind(infile);
	
	while(fgets(line, lineLen, infile)!=NULL){
		if(strstr(line, term)!=NULL){
			fgets(line, lineLen, infile);
			char *p = strtok(line, " \t\n");
			gold[atoi(p)] = 1;
			while(1){
				p = strtok(NULL, " \t\n");
				if(p==NULL) break;
				gold[atoi(p)] = 1;
			}
			break;
		}
	}
	fclose(infile);
	return gold;
}


void save_norm_matrix(short** new_m, short** data, int *numCoords, int *numObjs){
	int i, j;

	for (i = 0; i < *numObjs; i++){
		if (data[i][0] == -9999){
			new_m[i][0] = -9999;
		}else {
			float mean = 0;
			float stdev = 0;

			for (j = 0; j < *numCoords; j++){
				mean += (float) (data[i][j] / 100.0);
			}

			mean /= (float) *numCoords;
			float sum_dev = 0;

			for (j = 0; j < *numCoords; j++){
				float diff = (float) (data[i][j] / 100.0 - mean);
				sum_dev += diff * diff;
			}

			stdev = sqrt(sum_dev / (float) *numCoords);

			for (j = 0; j < *numCoords; j++){
				new_m[i][j] = (short) ((data[i][j] / 100.0 - mean) / stdev * 100.0);
				if(new_m[i][j]>32000){
					new_m[i][j]=32000;
				}else if(new_m[i][j]<-32000){
					new_m[i][j]= -32000;
				}
			}

		}
	}
}

int get_num_query(char *filename){
	FILE *infile;

	char *line, *ret;
	int len;

	int lineLen = MAX_CHAR_PER_LINE;
	int numQ = 0;

	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error: no such file (%s)\n", filename);
		return -1;
	}

	line = (char*) malloc(lineLen);
	assert(line != NULL);

	while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);

			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			assert(line != NULL);

			ret = fgets(line, lineLen, infile);
			assert(ret != NULL);
		}

		if (strtok(line, " \t\n") != 0){
			numQ++;
		}
	}

	rewind(infile);
	fclose(infile);
	free(line);
	return numQ;
}


struct query_union* queries_read(char *filename, int *numQueries){
	FILE *infile;
	struct query_union *qu = (struct query_union*)malloc(*numQueries * sizeof(struct query_union));
	//struct query_group** q;

	char *line, *ret;
	int len;
	int i, j, k;

	int lineLen = MAX_CHAR_PER_LINE;

	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error: no such file (%s)\n", filename);
		return NULL;
	}

	line = (char*) malloc(lineLen);
	assert(line != NULL);

	while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			assert(line != NULL);

			ret = fgets(line, lineLen, infile);
			assert(ret != NULL);
		}
	}

	rewind(infile);

	//query_length = (int*) malloc((*numQueries)*sizeof(int));

	/*if (fgets(line, lineLen, infile) != NULL){
		(*query_length)++;
		strtok(line, " \t\n");

		while (strtok(NULL, " ,\t\n") != NULL){
			(*query_length)++;
		}
	}*/

	//rewind(infile);
	//printf("length of query is %d\n", *query_length);
	//q = (struct query_group**) malloc((*numQueries) * sizeof(struct query_group*));

	i = 0;
	j = 0;

	while (fgets(line, lineLen, infile) != NULL){
		char *line2 = (char*)malloc(100000*sizeof(char));
		char *line3 = (char*)malloc(100000*sizeof(char));
		strcpy(line2, line);
		strcpy(line3, line);
		struct query_union *quu = &qu[i];
		quu->num_search_dset=0;

		if(strstr(line3, "|")!=NULL){
		char *px = strtok(line3, "|\n");
		strcpy(line2, px);
		strcpy(line, px);
		px = strtok(NULL, "|\n");
		
		char bx[100000];
		char by[100000];
		strcpy(bx, px);
		strcpy(by, px);

		char *py = strtok(bx, " \n");
		while (py!=NULL){
			quu->num_search_dset++;
			py = strtok(NULL, " \n");
		}

		quu->search_dset = (int*)malloc(quu->num_search_dset*sizeof(int));
		int* quu_s = quu->search_dset;
		py = strtok(by, " \n");
		k = 0;
		while (py!=NULL){
			quu_s[k] = atoi(py);
			k++;
			py = strtok(NULL, " \n");
		}
		}
			
		char *ppy = strtok(line, ";\n");
		j = 0; 
		while (ppy!=NULL){
			ppy= strtok(NULL, ";\n");
			j++;
		}
		
		//query_length[i] = j;
		quu->num_query_group = j;
		quu->qg = (struct query_group*) malloc(quu->num_query_group * sizeof(struct query_group));
		struct query_group *qi = quu->qg;

		//q[i] = (struct query_group*) malloc((query_length[i]) * sizeof(struct query_group));
		char *p = strtok(line2, ";\n");
		char **qq = (char**)malloc(j*sizeof(char*));

		j=0;

		while (p != NULL){
			qq[j] = p;
			p = strtok(NULL, ";\n");
			j++;
		}

		for(j=0; j<quu->num_query_group; j++){
			int s = 0;
			char b[100000];
			strcpy(b, qq[j]);
			char *p2 = strtok(b, " ");
			while (p2 != NULL){
				p2 = strtok(NULL, " ");
				s++;
			}
			qi[j].num_query = s;
			qi[j].query = (int*)malloc(s*sizeof(int));
		}

		for(j=0; j<quu->num_query_group; j++){
			int s = 0;
			char *p2 = strtok(qq[j], " ");
			while (p2 != NULL){
				qi[j].query[s] = atoi(p2);
				p2 = strtok(NULL, " ");
				s++;
			}
		}

		free(qq);
		free(line2);
		free(line3);
		i++;
	}

	fclose(infile);
	free(line);

	return qu;
}

struct dataset* dataset_read(char *filename, int *numObjs){
	FILE *infile;
	struct dataset* d;

	char *line, *ret;
	int len;
	int i;

	int lineLen = MAX_CHAR_PER_LINE;

	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error: no such file (%s)\n", filename);
		return NULL;
	}

	line = (char*) malloc(lineLen);
	assert(line != NULL);

	//numObjs is changed
	(*numObjs) = 0;

	while (fgets(line, lineLen, infile) != NULL){
		/* check each line to find the max line length */
		while (strlen(line) == lineLen - 1){
			/* this line read is not complete */
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);

			/* increase lineLen */
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			assert(line != NULL);

			ret = fgets(line, lineLen, infile);
			assert(ret != NULL);
		}

		if (strtok(line, " \t\n") != 0){
			(*numObjs)++;
		}
	}

	rewind(infile);

	//printf("%d\n", *numObjs);

	ret = fgets(line, lineLen, infile);
	if(line[0]=='g' && line[1]=='e' && line[2]=='n' && line[3]=='e'){
	}else{
		rewind(infile);
	}

	d = (struct dataset*) malloc((*numObjs) * sizeof(struct dataset));

	i = 0;

	while (fgets(line, lineLen, infile) != NULL){
		char *p = strtok(line, " \t\n");
		d[i].id = atoi(p);
		strcpy(d[i].name, strtok(NULL, " ,\t\n"));
		//printf("%d %s\n", d[i].id, d[i].name);
		i++;
	}

	fclose(infile);
	free(line);

	return d;

}

struct dataset* sinfo_read(char *filename, struct dataset* d){
	FILE *infile;

	char *line;
	int i;

	int lineLen = MAX_CHAR_PER_LINE;

	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error: no such file (%s)\n", filename);
		return NULL;
	}

	line = (char*) malloc(lineLen);
	assert(line != NULL);
	char *re; //return code

	for (i = 0; i < 6; i++){

		re = fgets(line, lineLen, infile);
		char *p = strtok(line, " \t\n");
		char *p1 = strtok(NULL, " \t\n");
		char *p2 = strtok(NULL, " \t\n");

		if (strcmp(p, "All") == 0 || strcmp(p, "All:")==0){
			d->zmean = atof(p1);
			d->zstdev = atof(p2);

			if (isnan(d->zmean) || isnan(d->zstdev)){
				printf("WARNING: z_mean/z_stdev is nan for Dset %d. It's not used.\n",
					d->index + 1);
			}

			//printf("%f %f\n", d->zmean, d->zstdev);
		}
	}

	free(line);
	fclose(infile);
	return d;
}

#ifdef GENE_STD
struct dataset* sinfo2_read(char *filename, struct dataset* d){
	FILE *infile;

	char *line;
	int i;

	int lineLen = MAX_CHAR_PER_LINE;

	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error: no such file (%s)\n", filename);
		return NULL;
	}

	line = (char*) malloc(lineLen);
	assert(line != NULL);
	char *re; //return code
	int numLines=0;

	while (fgets(line, lineLen, infile) != NULL){
		if (strtok(line, " \t\n") != 0){
			numLines++;
		}
	}
	rewind(infile);

	d->zmean_g = (float*)malloc(numGenes*sizeof(float));
	d->zstdev_g = (float*)malloc(numGenes*sizeof(float));

	for(i=0; i<numGenes; i++){
		d->zmean_g[i] = -9999;
		d->zstdev_g[i] = -9999;
	}

	for (i = 0; i < numLines; i++){
		re = fgets(line, lineLen, infile);
		char *p = strtok(line, " \t\n"); 
		char *p1 = strtok(NULL, " \t\n");
		char *p2 = strtok(NULL, " \t\n");
		int g = atoi(p);

		d->zmean_g[g] = atof(p1);
		d->zstdev_g[g] = atof(p2);
	}

	free(line);
	fclose(infile);
	return d;

}
#endif

short** file_read(char *filename, int *numObjs, int *numCoords){

	short **objects;
	FILE *infile;
	char *line, *ret;
	int len;
	int i, j;

	int lineLen = MAX_CHAR_PER_LINE;

	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error: no such file (%s)\n", filename);
		return NULL;
	}

	line = (char*) malloc(lineLen);
	assert(line != NULL);

	//numObjs is changed
	(*numObjs) = 0;

	//printf("Here");
	while (fgets(line, lineLen, infile) != NULL){
		/* check each line to find the max line length */
		while (strlen(line) == lineLen - 1){
			/* this line read is not complete */
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);

			/* increase lineLen */
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			assert(line != NULL);

			ret = fgets(line, lineLen, infile);
			assert(ret != NULL);
		}

		if (strtok(line, " \t\n") != 0){
			(*numObjs)++;
		}
	}

	//(*numObjs)--; //subtract header line

	rewind(infile);
	//printf("lineLen = %d\n",lineLen);

	//fgets(line, lineLen, infile); //skip first line, the header

	/* find the no. objects of each object */
	(*numCoords) = 0;

	while (fgets(line, lineLen, infile) != NULL){
		if (strtok(line, " \t\n") != 0){
			//(*numCoords)++;
			/* ignore the id (first coordiinate): numCoords = 1; */
			while (strtok(NULL, " ,\t\n") != NULL){
				(*numCoords)++;
			}

			break;     /* this makes read from 1st object */
		}
	}


	rewind(infile);
	//QIAN corrected
	(*numObjs) = numGenes;

	//fgets(line, lineLen, infile); //skip first line, the header

	//printf("File %s numObjs   = %d\n",filename,*numObjs);
	//printf("File %s numCoords = %d\n",filename,*numCoords);

	/* allocate space for objects[][] and read all objects */
	len = (*numObjs) * (*numCoords);
	objects    = (short**) malloc((*numObjs) * sizeof(short*));
	assert(objects != NULL);
	objects[0] = (short*) malloc(len * sizeof(short));
	assert(objects[0] != NULL);

	for (i = 1; i < (*numObjs); i++){
		objects[i] = objects[i - 1] + (*numCoords);
	}

	//Initialize
	for (i = 0; i < (*numObjs); i++){
		for (j = 0; j < (*numCoords); j++){
			objects[i][j] = -9999;
		}
	}


	i = 0;

	/* read all objects */
	while (fgets(line, lineLen, infile) != NULL){
		char *p = strtok(line, " \t\n");
		int ind = atoi(p);

		//objects[i][0] = atof(p);
		if (p == NULL){
			continue;
		}

		//id[i] = atoi(p);

		for (j = 0; j < (*numCoords); j++){
			objects[ind][j] = (short) (atof(strtok(NULL, " ,\t\n")) * 100.0);
		}

		// i++;
	}

	int countLen = 0;

	for (i = 0; i < (*numObjs); i++){
		if (objects[i][0] == -9999){
			countLen++;
			continue;
		}else {
			countLen += (*numCoords);
		}
	}

	short **obj2 = (short**) malloc((*numObjs) * sizeof(short*));
	obj2[0] = (short*) malloc(countLen * sizeof(short));

	for (i = 1; i < (*numObjs); i++){
		if (objects[i - 1][0] == -9999){
			obj2[i] = obj2[i - 1] + 1;
		}else {
			obj2[i] = obj2[i - 1] + (*numCoords);
		}
	}

	for (i = 0; i < (*numObjs); i++){
		if (objects[i][0] == -9999){
			obj2[i][0] = -9999;
		}else {
			for (j = 0; j < (*numCoords); j++){
				obj2[i][j] = objects[i][j];
			}
		}

	}

	fclose(infile);
	free(line);

#ifndef DO_NOT_CALC_NORM
	short **obj_n = (short**) malloc((*numObjs) * sizeof(short*));
	obj_n[0] = (short*) malloc(countLen * sizeof(short));

	for (i = 1; i < (*numObjs); i++){
		if (objects[i - 1][0] == -9999){
			obj_n[i] = obj_n[i - 1] + 1;
		}else {
			obj_n[i] = obj_n[i - 1] + (*numCoords);
		}
	}

	save_norm_matrix(obj_n, obj2, numCoords, numObjs);
	free(obj2[0]);
	free(obj2);
	free(objects[0]);
	free(objects);

	return obj_n;
#else

	free(objects[0]);
	free(objects);
	return obj2;
#endif

}
