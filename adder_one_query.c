#include "adder_one_query.h"


float **prepare_gene_map(
	struct Presence *absent_G, struct Presence *absent_Q, struct Map *map_G,
	const struct Presence *present_g, const struct Map *map_Q)
{

	#ifdef DEBUG
	if(absent_G == NULL || absent_Q == NULL || map_G==NULL){
		fprintf(stderr, "Error in prepare_gene_map: null\n"); 
		exit(1);
	}
	#endif

	clear_Presence(absent_G);
	clear_Presence(absent_Q);
	clear_Map(map_G);

	int ii, i, jj, j;	
	int num_unique_q = map_Q->num_set;

	for(jj=0; jj<num_unique_q; jj++){
		j = get_Map_r(jj, map_Q);
		if(check_bit(j, present_g)==0){
			set_bit(j, absent_Q);
			//printf("Good %d", j); getchar();
		}else{
			clear_bit(j, absent_Q);
		}
	}

	//printf("Dataset %d\n", d);
	for(i=0; i<numGenes; i++){
		if(get_Map_f(i, map_Q)!=-1){
			clear_bit(i, absent_G);
			continue;
		}
		if(check_bit(i, present_g)==0){
			set_bit(i, absent_G);
		}else{
			//printf("Present gene %d\n", i);
			clear_bit(i, absent_G);
		}
	}

	set_Map_from_Presence_reverse(absent_G, map_G);
	int numGenesD = map_G->num_set;
	float **f = init_float_2d(numGenesD, num_unique_q, 0);
	return f;
  
}


float **prepare_one_query_in_one_dataset_metacor(
	struct Presence *absent_G, struct Presence *absent_Q, struct Map *map_G,
	const struct Presence *present_g, const struct Map *map_Q,
	unsigned char **rd, 
	const struct Float2D *avg,
	const struct sinfo *si, const int d)
{

	float **f = prepare_gene_map(absent_G, absent_Q, map_G, present_g, map_Q);

	int ii, i, jj, j;	
	for(ii=0; ii<map_G->num_set; ii++){
		i = get_Map_r(ii, map_G);
		for(jj=0; jj<map_Q->num_set; jj++){
			j = get_Map_r(jj, map_Q);
			if(i==j){
				f[ii][jj] = 0;
			}
			if(rd[j]==NULL) continue;
			if(rd[j][i] != 255){
				float rf = (float) rd[j][i];
				float rff = rf / 10.0 - 12.0;
				//ADD AVERAGE================
				rff = rff + get_Float2D_xy(d, i, avg);
				//===========================
				rff = rff * si[d].stdev + si[d].mean;
				rff = exp(rff * 2.0);
				rff = (rff - 1.0) / (rff + 1.0);
				f[ii][jj] = rff;
			}

		}
	}
	return f;
}

//for NO_SUBAVGZ
float ** prepare_one_query_in_one_dataset_no_subavgz(
	struct Presence *absent_G, struct Presence *absent_Q, struct Map *map_G,
	const struct Presence *present_g, const struct Map *map_Q,
	unsigned char **rd, 
	const struct Float2D *avg, const int d)
{
	float **f = prepare_gene_map(absent_G, absent_Q, map_G, present_g, map_Q);

	int ii, i, jj, j;	
	for(ii=0; ii<map_G->num_set; ii++){
		i = get_Map_r(ii, map_G);
		for(jj=0; jj<map_Q->num_set; jj++){
			j = get_Map_r(jj, map_Q);
			if(i==j){
				f[ii][jj] = 0;
			}
			if(rd[j]==NULL) continue;
			if(rd[j][i] != 255){
				float rf = (float) rd[j][i];
				float rff = rf / 10.0 - 12.0;
				rff += get_Float2D_xy(d, i, avg);
				//f[ii][jj] = rff;
				/*if(rff<0){
					f[ii][jj] = 0;
				}*/
				f[ii][jj] = (rff + 12.0) * 10.0;
			}
		}
	}
	return f;
}

//for everything else
float **prepare_one_query_in_one_dataset_normal(
	struct Presence *absent_G, struct Presence *absent_Q, struct Map *map_G,
	const struct Presence *present_g, const struct Map *map_Q,
	unsigned char **rd)
{
	float **f = prepare_gene_map(absent_G, absent_Q, map_G, present_g, map_Q);
 
	int ii, i, jj, j;	
	for(ii=0; ii<map_G->num_set; ii++){
		i = get_Map_r(ii, map_G);
		for(jj=0; jj<map_Q->num_set; jj++){
			j = get_Map_r(jj, map_Q);
			if(i==j){
				f[ii][jj] = 0;
			}
			if(rd[j]==NULL) continue;
			if(rd[j][i] != 255){
				float rf = (float) rd[j][i];
				f[ii][jj] = rf;
				//f[ii][jj] = rf / 10.0 - 12.0;
				//f[ii][jj] = (f[ii][jj] + 12.0) * 10.0;

				//printf("Query %d Gene %d: %.2f Platform %.2f\n", j, i, f[ii][jj], platform_mean);
				
				/*f[ii][jj] = rf / 10.0 - 12.0;
				if(rf<0){
					f[ii][jj] = 0;
				}*/

				//COMMENTED Apr 1
				//s[jj] += rf;
				/*rf = (float)(r[d][j][i]) / 10.0 - 12.0;
				if(rf>1.0){
					f[ii][jj] = rf;
					//f[i][jj] = rf*rf; //MOD
				}else{
					f[ii][jj] = 0;
					//f[i][jj] = 0; //MOD
				}*/
				//f[i][jj] = get_percentile(rf);
				/*if(r[d][j][i]==0){
					f[ii][jj] = 0;
				}else{
					f[ii][jj] = (float) (r[d][j][i]) / 10.0 - 12.0;
				}*/
				//#endif
			}

		}
	}
	return f;
}

