/*
 * linear_combination.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */

#include "linear_combination.h"

struct Float2D * linear_combine(struct Float2D *w, float ***res_cross){

	int i, j, k, l;
	int jj;
	int numQueries = w->d1;
	int numDsetLoad = w->d2;
	struct Float2D *fr = init_Float2D(numQueries, numGenes, 0);

	for(l=0; l<numQueries; l++){
		for(j=0; j<numDsetLoad; j++){
			for(k=0; k<numGenes; k++){
				increment_Float2D_xy(l, k, get_Float2D_xy(l, j, w)
					* res_cross[l][j][k], fr);
			}
		}
	}
	return fr;
}

void linear_combine_within(int l, float w, float *rank_normal, struct Float2D *fr){
	int k;
	for(k=0; k<numGenes; k++){
		increment_Float2D_xy(l, k, rank_normal[k] * w, fr);
	}
}
