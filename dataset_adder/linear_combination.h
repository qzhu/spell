/*
 * linear_combination.h
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */

#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif

#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

struct Float2D * linear_combine(struct Float2D *w, float ***res_cross);
void linear_combine_within(int l, float w, float *rank_normal, struct Float2D *fr);
