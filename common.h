#include <gsl/gsl_rng.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector_float.h>
#include <gsl/gsl_sort_float.h>
#include <gsl/gsl_sort_vector_float.h>
#include <gsl/gsl_permute_vector_float.h>

#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "adder_basic.h"
#endif

#ifndef GENE_PRESENCE
#define GENE_PRESENCE
#include "gene_presence.h"
#endif

struct Float2D{
	float **f;
	int d1;
	int d2;
	char is_matrix;
	char is_compacted;
	int *d2_size;
	int tot;
};

float *init_float(int size, float def);
float **init_float_2d(int size1, int size2, float def);
float ***init_float_3d(int size1, int size2, int size3, float def);
void clear_float_2d(float **w, int size1, int size2);
void clear_float(float *w, int size);
void free_float_2d(float **w);
void free_float_3d(float ***w);

int *init_int(int size, int def);
int **init_int_2d(int size1, int size2, int def);
int ***init_int_3d(int size1, int size2, int size3, int def);
void clear_int_2d(int **w, int size1, int size2);
void clear_int(int *w, int size);
void free_int_2d(int **w);
void free_int_3d(int ***w);

struct Float2D* init_Float2D(int d1, int d2, float def_val);
struct Float2D* init_Float2D_x(int d1);
struct Float2D* init_Float2D_y(int at_x, int d2, float def_val, struct Float2D *fd);
void free_Float2D(struct Float2D *fd);
float get_Float2D_xy(int x, int y, const struct Float2D *fd);
float* get_Float2D_x(int x, const struct Float2D *fd);
void set_Float2D_xy(int x, int y, float v, struct Float2D *fd);
void increment_Float2D_xy(int x, int y, float v, struct Float2D *fd);
struct Float2D* compact_Float2D(struct Float2D *fd);
FILE * FOpen(char *filename, const char * mode);


//integer to integer mapping
struct Map{
	int *f;
	int *r;
	int size;
	int num_set;
};

struct Map* init_Map(int size);
struct Map* init_Map_2d(int size, int size2);
struct Map* init_Map_from_Presence(const struct Presence *p);
struct Map* init_Map_from_Presence_reverse(const struct Presence *p);
void set_Map_from_Presence(const struct Presence *p, struct Map *m);
void set_Map_from_Presence_reverse(const struct Presence *p, struct Map *m);

void free_Map(struct Map *m);
void free_Map_2d(struct Map *m, int size);
void clear_Map(struct Map *m);
int get_Map_f(int i, const struct Map *m);
int get_Map_r(int i, const struct Map *m);
void add_to_Map(int g, struct Map *m);

//string to integer mapping, works for one-to-one mapping only
struct StrIntMap{
	rb_red_blk_tree *f; //str to int
	rb_red_blk_tree *r; //int to str
	int size;	
	int str_size;
};

struct StrIntMap *init_StrIntMap();
struct StrIntMap* read_StrIntMap(char *map_file);
void set_StrIntMap(char *name, int id, struct StrIntMap *tree);
int get_StrIntMap_int(char *name, struct StrIntMap *tree);
char* get_StrIntMap_str(int id, struct StrIntMap *tree);
char** get_StrIntMap_all_keys_str(struct StrIntMap *tree);
int* get_StrIntMap_all_keys_int(struct StrIntMap *tree);
void free_StrIntMap(struct StrIntMap *tree);

struct Platform{
	struct StrIntMap *m; //dataset name to id mapping
	char *platform_name;
	int platform_id;
};

struct Platform *init_Platform();

struct Platforms{
	struct Platform *platform;
	struct StrIntMap *m; //platform name to id mapping
	int numPlatforms;
	char *bin_path;
	char *dataset_conv_path;
	char *sinfo_path;
	char *var_path;
	char *avg_path;
	char *platform_avg_path;
};

struct StrIntMap *merge_Platform_mapping(struct Platforms *ps);



