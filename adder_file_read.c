#include "adder_file_read.h"

int GPL96 = 0;
int GPL91 = 1;
int GPL570 = 2;
int GPL97 = 3;

int SIZES[] = {0, 0, 0, 0};


rb_red_blk_tree* readGeneMap(char *gene_map_file){
	FILE *ft = fopen(gene_map_file, "r");
	rb_red_blk_tree *gene_tree = RBTreeCreate(CharArrayComp, 
		CharArrayDest, InfoDest, CharArrayPrint, InfoPrint);

	char *line, *ret;
	int len;
	int lineLen = MAX_CHAR_PER_LINE;
	line = (char*)malloc(lineLen);

	while (fgets(line, lineLen, ft) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(ft, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, ft);
		}
	}

	rewind(ft);
	while (fgets(line, lineLen, ft) != NULL){
		char *pp = (char*)malloc(100);
		char *p = strtok(line, " \t\n");
		sprintf(pp, "%s", p);
		int *id = (int*)malloc(sizeof(int));
		*id = atoi(strtok(NULL, " \t\n"));
		RBTreeInsert(gene_tree, pp, id);
	}
	fclose(ft);
	free(line);

	return gene_tree;
}

int get_gene_id(char *name, rb_red_blk_tree* gene_tree){
	rb_red_blk_node *node1 = RBExactQuery(gene_tree, name);
	int i;
	if(node1==0){
		printf("Error, gene %s is not found", name);
		getchar();
		return -1;
	}else{
		int *ip = (int*)node1->info;
		i = *ip;
		return i;
	}
}

float* read_stddev(char *name, rb_red_blk_tree *gene_tree){
	float *g = (float*)malloc(numGenes*sizeof(float));
	int i;

	for(i=0; i<numGenes; i++){
		g[i] = 0;
	}

	FILE* infile;
	if ((infile = fopen(name, "r")) == NULL){
		fprintf(stderr, "Error no such file \n");
		return NULL;
	}
	int lineLen = MAX_CHAR_PER_LINE;
	char *line, *ret;
	line = (char*) malloc(lineLen);
	int len;
	while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}
	}

	rewind(infile);

	while (fgets(line, lineLen, infile) != NULL){
		char *p = strtok(line, "\t\n");
		float v = atof(strtok(NULL, "\t\n"));
		int gi = get_gene_id(p, gene_tree);
		if(gi==-1){
			continue;
		}
		g[gi] = v;
	}
	fclose(infile);
	free(line);

	return g;
}

char** presence_read(){
	FILE *infile;
	char *line, *ret;
	int len;
	int i, j, k;

	if ((infile = fopen("dataset_presence.txt", "r")) == NULL){
		fprintf(stderr, "Error no such file \n");
		return NULL;
	}

	int lineLen = MAX_CHAR_PER_LINE;
	line = (char*) malloc(lineLen);

	while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}
	}

	rewind(infile);

	char** q = (char**) malloc(numDatasets * sizeof(char*));
	q[0] = (char*) malloc(numDatasets * numGenes * sizeof(char));

	for (i = 1; i < numDatasets; i++){
		q[i] = q[i - 1] + numGenes;
	}

	for (i = 0; i < numDatasets; i++){
		for (j = 0; j < numGenes; j++){
			q[i][j] = 0;
		}
	}

	while (fgets(line, lineLen, infile) != NULL){
		char *p = strtok(line, ": \t\n");
		int dset = atoi(p);
		p = strtok(NULL, ": \t\n");

		while (1){
			if (p == NULL){
				break;
			}

			int gene = atoi(p);
			q[dset][gene] = 1;
			p = strtok(NULL, ": \t\n");
		}
	}

	fclose(infile);
	free(line);

	return q;
}

int find_gold_std_id(char *n, struct goldstd *g){
	int i;
	for(i=0; i<g->num_gold_std; i++){
		//printf("%d %s\n", i, g->term_name[i]); getchar();
		if(strcmp(g->term_name[i], n)==0){
			break;
		}
	}
	if(i==g->num_gold_std){
		return -1;
	}else{
		return i;
	}
}

void free_goldstd(struct goldstd *g){
	free(g->gold[0]);
	free(g->gold);
	free(g->term_name[0]);
	free(g->term_name);
	free(g->size);
	free(g);
}

struct goldstd * gold_standard_read(char *file){
	FILE *infile;
	char *line, *ret;
	int len;
	int i, j, k;
	int lineLen = MAX_CHAR_PER_LINE;
	//char term[50];
	//sprintf(term, "QZ:%s", _term);

	//if ((infile = fopen("all_non_overlap_terms_mapped.sym.num", "r")) == NULL){
	if ((infile = fopen(file, "r")) == NULL){
		fprintf(stderr, "Error no such file \n");
		return NULL;
	}

	line = (char*) malloc(lineLen);
	int numLines = 0;

	while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}
		numLines++;
	}

	int NUM_GOLD_STD = numLines / 2;

	char **gold = (char**) malloc(NUM_GOLD_STD * sizeof(char*));
	gold[0] = (char*)malloc(numGenes*NUM_GOLD_STD*sizeof(char));
	for(i=1; i<NUM_GOLD_STD; i++){
		gold[i] = gold[i-1] + numGenes;
	}

	int length_each_term = 200;
	char **term_name = (char**)malloc(NUM_GOLD_STD * sizeof(char*));
	term_name[0] = (char*)malloc(length_each_term*NUM_GOLD_STD*sizeof(char));
	for(i=1; i<NUM_GOLD_STD; i++){
		term_name[i] = term_name[i-1] + length_each_term;
	}

	for(j=0; j<NUM_GOLD_STD; j++){
		for (i = 0; i < numGenes; i++){
			gold[j][i] = 0;
		}
	}

	rewind(infile);

	i=0;
	while (fgets(line, lineLen, infile) != NULL){
		//term line
		char *p = strtok(line, "\n");
		strcpy(term_name[i], p);
		//sprintf(term_name[i], "%s", p); 
		//printf("%s\n", term_name[i]);

		ret = fgets(line, lineLen, infile);
		p = strtok(line, " \t\n");
		int gid = atoi(p);
		if(gid>=numGenes){
			printf("invalid gid %d", gid);
			getchar();
		}
		gold[i][gid] = 1;

		while (1){
			p = strtok(NULL, " \t\n");

			if (p == NULL){
				break;
			}
			int gid = atoi(p);
			if(gid>=numGenes){
				printf("invalid gid %d", gid);
				getchar();
			}

			gold[i][gid] = 1;
		}
		i++;

	}

	struct goldstd *gs = (struct goldstd *)malloc(sizeof(struct goldstd));
	gs->gold = gold;
	gs->term_name = term_name;
	gs->num_gold_std = NUM_GOLD_STD;


	gs->size = (int*)malloc(NUM_GOLD_STD*sizeof(int));

	for(j=0; j<NUM_GOLD_STD; j++){
		gs->size[j] = 0;
		for(i=0; i<numGenes; i++){
			if(gold[j][i]==1){
				gs->size[j]++;
			}
		}
	}


	fclose(infile);
	return gs;
}

struct rndgenerator * read_random_numbers(char *file){
	FILE *infile;
	char *line, *ret;
	int len;
	int i, j, k;
	int lineLen = MAX_CHAR_PER_LINE;
	struct rndgenerator *s1 = (struct rndgenerator*)malloc(sizeof(struct rndgenerator));

	if ((infile = fopen(file, "r")) == NULL){
		fprintf(stderr, "Error no such file %s\n", file);
		return NULL;
	}

	line = (char*) malloc(lineLen);
	int numLines = 0;

	while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}
		numLines++;
	}

	int num = numLines;
	s1->num = num;
	s1->current = 0;
	s1->r = (float*)malloc(num*sizeof(float));

	rewind(infile);
	i = 0;
	while (fgets(line, lineLen, infile) != NULL){
		char *p = strtok(line, "\n");
		s1->r[i] = atof(p);
		i++;
	}

	free(line);
	fclose(infile);
	return s1;
}

float get_random_number(struct rndgenerator *r){
	if(r->current==r->num){
		r->current = 0;
	}
	float f = r->r[r->current];
	r->current = r->current + 1;
	return f;
}

char* dset_info_read(){
	FILE *infile;
	char *line, *ret;
	int len;
	int i, j, k;
	int lineLen = MAX_CHAR_PER_LINE;

	if ((infile = fopen("gpl96_570_91_97.txt", "r")) == NULL){
		fprintf(stderr, "Error no such file \n");
		return NULL;
	}

	line = (char*) malloc(lineLen);

	while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);

			if (strstr(line, "GPL570") != NULL){
				SIZES[GPL570]++;
			}else if (strstr(line, "GPL96") != NULL){
				SIZES[GPL96]++;
			}else if (strstr(line, "GPL91") != NULL){
				SIZES[GPL91]++;
			}else if (strstr(line, "GPL97") != NULL){
				SIZES[GPL97]++;
			}
		}
	}

	rewind(infile);
	char *dset = (char*) malloc(numDatasets * sizeof(char));

	for (i = 0; i < numDatasets; i++){
		dset[i] = -1;
	}

	while (fgets(line, lineLen, infile) != NULL){
		char *p = strtok(line, "\t\n");
		char *p2 = strtok(NULL, "\t\n");

		if (strstr(p2, "GPL570") != NULL){
			dset[atoi(p)] = GPL570;
		}else if (strstr(p2, "GPL96") != NULL){
			dset[atoi(p)] = GPL96;
		}else if (strstr(p2, "GPL91") != NULL){
			dset[atoi(p)] = GPL91;
		}else if (strstr(p2, "GPL97") != NULL){
			dset[atoi(p)] = GPL97;
		}
	}

	fclose(infile);
	free(line);
	return dset;
}

int** query_read(char *term, int query_size){
	FILE *infile;
	char *line, *ret;
	int len;
	int i, j, k;
	char filename[500];
	//sprintf(filename, "queries/3-tuple/num2/QZ:%s", term);
	//sprintf(filename, "queries6/QZ:%s", term);
	//sprintf(filename, "good_queries/QZ:%s", term);

	sprintf(filename, "%s", term);

	int lineLen = MAX_CHAR_PER_LINE;

	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error no such file \n");
		return NULL;
	}

	line = (char*) malloc(lineLen);

	//============GLOBAL================
	numQueries = 0;
	//==================================

	while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}

		numQueries = numQueries + 1;
	}

	rewind(infile);

	int** q = (int**) malloc(numQueries * sizeof(int*));
	q[0] = (int*)malloc(numQueries*query_size*sizeof(int));
	for(i=1; i<numQueries; i++){
		q[i] = q[i-1] + query_size;
	}

	for (i = 0; i < numQueries; i++){
		//q[i] = (int*) malloc(query_size * sizeof(int));
		ret = fgets(line, lineLen, infile);
		char *p = strtok(line, " \t\n");
		q[i][0] = atoi(p);

		for (j = 1; j < query_size; j++){
			p = strtok(NULL, " \t\n");
			q[i][j] = atoi(p);
		}
	}

	fclose(infile);
	free(line);

	return q;
}



float** pagerank_read(char *file, float **r){
	FILE *infile;
	char *ret;
	int len;
	int i, j, k, d, g;
	
	if((infile=fopen(file, "rb")) == NULL){
		fprintf(stderr, "Error no such file (%s)\n", file);
		return NULL;
	}

	char dset[4];
	for(i=1; i<strlen(file); i++){
		if(file[i]=='.'){
			dset[i-1] = '\0';
			break;
		}
		dset[i-1] = file[i];
	}
	int ds = atoi(dset);

	//printf("%d\n", ds); getchar();

	if(r==NULL){
		r = (float**)malloc(numDatasets*sizeof(float*));
		r[0] = (float*)malloc(numDatasets*numGenes*sizeof(float));
		for(i=1; i<numDatasets; i++){
			r[i] = r[i-1] + numGenes;
			for(j=0; j<numGenes; j++){
				r[i][j] = -9999;
			}
		}
	}

	char *line = (char*)malloc(1024);
	char *p;
	while(fgets(line, 1024, infile)!=NULL){
		p = strtok(line, " \t\n");
		int gene = atoi(p);
		p = strtok(NULL, " \t\n");
		float score = atof(p);
		r[ds][gene] = score;
	}

	free(line);
	fclose(infile);

	return r;
}

unsigned char*** result_read_binary_normal(char *file, unsigned char*** r){
	FILE *infile;
	char filename[500];
	
	sprintf(filename, "%s", file);
	if ((infile = fopen(filename, "rb")) == NULL){
		fprintf(stderr, "Error no such file (%s)\n", filename);
		return NULL;
	}
	
	r = result_read_binary(infile, r);
	fclose(infile);	
	return r;
}

unsigned char*** result_read_binary_stdin(unsigned char*** r){
	FILE *infile;

	//infile = stdin;
	infile = freopen(NULL, "rb", stdin);

	r = result_read_binary(infile, r);
	fclose(infile);
	return r;
}

//assumes dsetID is smaller than numDatasets
//unsigned char*** result_read_binary(char *term){
unsigned char*** result_read_binary(FILE *infile, unsigned char ***conv){
	char *line, *ret;
	int len;
	int i, j, k, d, g;
	char c[4];
	size_t st;

	st = fread(c, 1, 4, infile);
	int num_dset = *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_gene = *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_entries = *((int*)c);
	int nd = num_dset;
	int ng = num_gene;

	//============================================
	numDatasets = nd; //change global variable
	numGenes = ng; //change global variable
	//============================================

	if(conv==NULL){
		conv = (unsigned char***)malloc(nd*sizeof(unsigned char**));
		conv[0] = (unsigned char**)malloc(nd*ng*sizeof(unsigned char*));
		for(i=1; i<nd; i++){
			conv[i] = conv[i-1] + ng;
		}

		for(i=0; i<nd; i++){
			for(j=0; j<ng; j++){
				conv[i][j] = NULL;
			}
		}
	}

	unsigned short *s;
	char *v = (char*)malloc(ng);
	unsigned char *us;


	int num_list = num_entries / ng;
	for(i=0; i<num_list; i++){
		st = fread(c, 1, 4, infile);
		s = (unsigned short*) c;
		d = *s;
		s++;
		g = *s;

		st = fread(v, 1, ng, infile);
		if(i==0){
			TMP1 = d;
			TMP2 = g;
			conv[d][g] = (unsigned char*)malloc(num_entries * sizeof(unsigned char));
			us = conv[d][g];
		}else{
			conv[d][g] = us + ng;
			us = conv[d][g];
		}
		for(j=0; j<ng; j++){
			conv[d][g][j] = *(((unsigned char*) v) + j);
		}
	}

	free(v);

	return conv;
}

float** result_read_func_network(FILE *infile, float** conv_g, int gene){
	int len;
	int i, j, k, g;
	float d;
	char c[4];
	size_t st;

	st = fread(c, 1, 4, infile);
	int num_gene = *((int*)c);
	int ng = num_gene;

	//printf("NG: %d\n", ng); 

	if(conv_g==NULL){
		conv_g = (float**)malloc(numGenes*sizeof(float*));
		for(i=0; i<numGenes; i++){
			conv_g[i] = NULL;
		}
	}

	conv_g[gene] = (float*)malloc(numGenes*sizeof(float));
	for(i=0; i<numGenes; i++){
		conv_g[gene][i] = 0;
	}

	for(i=0; i<ng; i++){
		st = fread(c, 1, 4, infile);
		float *s = (float*) c;
		d = *s;
		conv_g[gene][i] = d;
	}

	return conv_g;
}

float** fix_func_network_g(float ** f){
	int i = 0;
	int j = 0;
	int g = 0;
	for(i=0; i<numGenes; i++){
		if(f[i]==NULL){
			continue;
		}
		g++;
	}

	float **f_new = (float**)malloc(numGenes*sizeof(float*));
	for(i=0; i<numGenes; i++){
		f_new[i] = NULL;
	}
	float *us = NULL;
	
	for(i=0; i<numGenes; i++){
		if(f[i]==NULL) continue;
		if(us==NULL){
			f_new[i] = (float*)malloc(numGenes*g*sizeof(float));
			us = f_new[i];
		}else{
			f_new[i] = us + numGenes; 
			us = f_new[i];
		}
		for(j=0; j<numGenes; j++){
			f_new[i][j] = f[i][j];
		}
	}

	for(i=0; i<numGenes; i++){
		if(f[i]==NULL) continue;
		free(f[i]);
	}
	free(f);

	return f_new;
}

unsigned char*** result_read_binary_by_g(FILE *infile, unsigned char ***conv_g){
	return result_read_binary_by_g_cust(infile, conv_g, 0);
}


unsigned char*** result_read_binary_by_g_cust(FILE *infile, unsigned char ***conv_g, int dset_incr){
	char *line, *ret;
	int len;
	int i, j, k, d, g;
	char c[4];
	size_t st;

	st = fread(c, 1, 4, infile);
	int num_dset = *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_gene = *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_entries = *((int*)c);
	int nd = num_dset;
	int ng = num_gene;

	//printf("%d %d %d", nd, ng, num_entries); getchar();
	//============================================
	numDatasets = nd; //change global variable
	numGenes = ng; //change global variable
	//============================================

	if(conv_g==NULL){
		conv_g = (unsigned char***)malloc(ng*sizeof(unsigned char**));
		conv_g[0] = (unsigned char**)malloc(nd*ng*sizeof(unsigned char*));
		for(i=1; i<ng; i++){
			conv_g[i] = conv_g[i-1] + nd;
		}
		for(i=0; i<ng; i++){
			for(j=0; j<nd; j++){
				conv_g[i][j] = NULL;
			}
		}
	}

	unsigned short *s;
	unsigned char *us = NULL;
	char *v = (char*)malloc(ng);

	int num_list = num_entries / ng;
	//printf("HH"); getchar();
	for(i=0; i<num_list; i++){
		st = fread(c, 1, 4, infile);
		s = (unsigned short*) c;
		d = *s;
		s++;
		g = *s;

		//add dset increment==============
		d += dset_incr;
		//================================

		st = fread(v, 1, ng, infile);
		//printf("%d %d", d, g); getchar();
		if(us==NULL){
			TMP1 = d;
			TMP2 = g;
			conv_g[g][d] = (unsigned char*)malloc(nd * ng * sizeof(unsigned char));
			us = conv_g[g][d];
		}else{
			conv_g[g][d] = us + ng;
			us = conv_g[g][d];
		}
		for(j=0; j<ng; j++){
			conv_g[g][d][j] = *(((unsigned char*) v) + j);
		}
	}

	free(v);

	return conv_g;
}

unsigned char*** fix_conv_g(unsigned char*** conv_g){
	int i, j, k;
	unsigned char *** conv_results =
		(unsigned char***) malloc(numDatasets * sizeof(unsigned char**));
	conv_results[0] =
		(unsigned char **) malloc(numDatasets * numGenes * sizeof(unsigned char*));

	for (j = 1; j < numDatasets; j++){
		conv_results[j] = conv_results[j - 1] + numGenes;
	}

	for (j = 0; j < numDatasets; j++){
		for (i = 0; i < numGenes; i++){
			conv_results[j][i] = NULL;
		}
	}

	long long int num_entries = 0;
	int num_list = 0;

	for (i = 0; i < numGenes; i++){
		for (j = 0; j < numDatasets; j++){
			if(conv_g[i][j]!=NULL){
				num_list++;
			}
		}
	}

	num_entries = (long long int) num_list * (long long int) numGenes;
	unsigned char *us = NULL;

	for (i = 0; i < numGenes; i++){

		for(j=0; j<numDatasets; j++){
			if (conv_g[i][j] != NULL){
				break;
			}
		}
		if(j==numDatasets){
			continue;
		}

		for (j = 0; j < numDatasets; j++){
			if (conv_g[i][j] == NULL){
				continue;
			}

			if (us == NULL){
				TMP1 = j;
				TMP2 = i;
				conv_results[j][i] =
					(unsigned char*) malloc(num_entries * sizeof(unsigned char));
				us = conv_results[j][i];
			}else {
				conv_results[j][i] = us + numGenes;
				us = conv_results[j][i];
			}

			for (k = 0; k < numGenes; k++){
				conv_results[j][i][k] = conv_g[i][j][k];
			}
		}
	}


	for (i = 0; i < numGenes; i++){
		for(j=0; j<numDatasets; j++){
			if (conv_g[i][j] != NULL){
				free(conv_g[i][j]);
				break;
			}
		}
	}
	free(conv_g[0]);
	free(conv_g);

	return conv_results;
}



unsigned char*** free_r(unsigned char*** c){
	free(c[TMP1][TMP2]);
	free(c[0]);
	free(c);
	return c;
}

unsigned char*** query_result_read_binary(FILE *infile, unsigned char ***conv){
	char *line, *ret;
	int len;
	int i, j, k, d, g, q;
	char c[4];
	int br;
	size_t st;

	st = fread(c, 1, 4, infile);
	int num_queries = *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_datasets= *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_genes= *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_entries = *((int*)c);
	int nd = num_datasets;
	int ng = num_genes;
	int nq = num_queries;

	//============================================
	numDatasets = nd; //change global variable
	numGenes = ng; //change global variable
	numQueries = nq; //change global variable
	//============================================

	if(conv==NULL){
		conv = (unsigned char***)malloc( nq * sizeof(unsigned char**) );
		conv[0] = (unsigned char**)malloc( nq * nd * sizeof(unsigned char*) );
		for(i=1; i<nq; i++){
			conv[i] = conv[i-1] + nd;
		}

		for(i=0; i<nq; i++){
			for(j=0; j<nd; j++){
				conv[i][j] = NULL;
			}
		}
	}

	unsigned short *s;
	char *v = (char*)malloc(ng);
	unsigned char *us;

	int num_list = num_entries / ng;
	for(i=0; i<num_list; i++){
		br = fread(c, 1, 4, infile);
		if(br!=4){
			printf("Error, number of bytes read %d, should be %d\n", br, 4);
			getchar();
			return NULL;
		}
		s = (unsigned short*) c;
		q = *s;
		s++;
		d = *s;
		br = fread(v, 1, ng, infile);

		if(br!=ng){
			printf("Error, number of bytes read %d, should be %d\n", br, ng);
			getchar();
			return NULL;
		}

		if(i==0){
			TMP1 = q;
			TMP2 = d;
			conv[q][d] = (unsigned char*)malloc(num_entries * sizeof(unsigned char));
			us = conv[q][d];
		}else{
			conv[q][d] = us + ng;
			us = conv[q][d];
		}
		for(j=0; j<ng; j++){
			conv[q][d][j] = *(((unsigned char*) v) + j);
		}
	}

	free(v);

	return conv;
}

//assumes dsetID is smaller than numDatasets
//unsigned char*** result_read_binary(char *term){
unsigned char*** result_read_binary_combine(char *file, unsigned char ***conv){
	FILE *infile;
	char *line, *ret;
	int len;
	int i, j, k, d, g;
	char filename[50];
	char c[4];
	size_t st;

	sprintf(filename, "%s", file);
	if ((infile = fopen(filename, "rb")) == NULL){
		fprintf(stderr, "Error no such file (%s)\n", filename);
		return conv;
	}
	
	/*
	infile = stdin;
	freopen(NULL, "rb", infile);
	*/

	st = fread(c, 1, 4, infile);
	int num_dset = *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_gene = *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_entries = *((int*)c);
	int nd = num_dset;
	int ng = num_gene;

	//============================================
	numDatasets = nd; //change global variable
	numGenes = ng; //change global variable
	//============================================

	if(conv==NULL){
		conv = (unsigned char***)malloc( nd * sizeof(unsigned char**) );
		conv[0] = (unsigned char**)malloc( nd * ng * sizeof(unsigned char*) );
		for(i=1; i<nd; i++){
			conv[i] = conv[i-1] + ng;
		}

		for(i=0; i<nd; i++){
			for(j=0; j<ng; j++){
				conv[i][j] = NULL;
			}
		}
	}

	unsigned short *s;
	char *v = (char*)malloc(ng);
	unsigned char *us;

	int num_list = num_entries / ng;
	for(i=0; i<num_list; i++){
		st = fread(c, 1, 4, infile);
		s = (unsigned short*) c;
		d = *s;
		s++;
		g = *s;
		st = fread(v, 1, ng, infile);
		if(i==0){
			conv[d][g] = (unsigned char*)malloc(num_entries * sizeof(unsigned char));
			us = conv[d][g];
		}else{
			conv[d][g] = us + ng;
			us = conv[d][g];
		}
		for(j=0; j<ng; j++){
			conv[d][g][j] = *(((unsigned char*) v) + j);
		}
	}

	free(v);
	fclose(infile);

	return conv;
}

float ** read_weight(FILE *infile, float** f, int dset){
	int i, j;
	char *ret; 
	if(f==NULL){
		f = (float**)malloc(numQueries*sizeof(float*));
		f[0] = (float*)malloc(numQueries*numDatasets*sizeof(float));
		for(i=1; i<numQueries; i++){
			f[i] = f[i-1] + numDatasets;
		}
		for(i=0; i<numQueries; i++){
			for(j=0; j<numDatasets; j++){
				f[i][j] = 0;
			}
		}
	}

	char *p;
	char *line = (char*)malloc(1024);

	int q = -1;
	float w1, w2;
	while (fgets(line, 1024, infile) != NULL){
		if(strstr(line, "Query")!=NULL){
			ret = fgets(line, 1024, infile);
			q++;
			p = strtok(line, " \n");
			w1 = atof(p);
			p = strtok(NULL, " \n");
			w2 = atof(p);
			f[q][dset] = w1;
			//printf("%.5f\n", f[q][dset]);
		}
	}
	free(line);

	return f;
}

//assumes dsetID is smaller than numDatasets
//unsigned char*** result_read(char *term){
unsigned char*** result_read(){
	FILE *infile;
	char *line, *ret;
	int len;
	int i, j, k;

	int lineLen = MAX_CHAR_PER_LINE;
	unsigned char *** results =
		(unsigned char***) malloc(numGenes * sizeof(unsigned char**));

	for (i = 0; i < numGenes; i++){
		results[i] = NULL;
	}

	char filename[50];
	//sprintf(filename, "single_gene/r_QZ_%s_single_gene.txt", term);

	//sprintf(filename, "may7_single_genes/QZ:%s", term);

	//sprintf(filename, "may18_clustered_genes/k10/QZ:%s", term);
	//sprintf(filename, "single_gene_nc/QZ:%s", term);
	//sprintf(filename, "single_gene_nc_glb/QZ:%s", term);

	/*
	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error no such file (%s)\n", filename);
		return NULL;
	}*/
	infile = freopen(NULL, "r", stdin);

	//=====================================================
	//assume each gene-score pair takes up 11 characters:
	//e.g.: "25000:-3.4 "
	//=====================================================
	//lineLen = 275000;
	lineLen = (int) (numGenes * 1.1) * 11;
	line = (char*) malloc(lineLen);

	/*while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}
	}
	rewind(infile);
	printf("%d\n", lineLen);
	*/
	//printf("Got here"); getchar();	
	
	int query;
	int dataset;
	int num_entries = 0;
	int num_query = 0;

	while (fgets(line, lineLen, infile) != NULL){

		/*while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}*/
		//printf("Here\n"); 

		char *line2 = (char*) malloc(lineLen * sizeof(char));
		char *line3 = (char*) malloc(lineLen * sizeof(char));
		strcpy(line2, line);
		strcpy(line3, line);

		if (strstr(line2, "Query") != NULL){
			char *p = strstr(line2, ":");
			p = p + 1;
			query = atoi(strtok(p, " \t\n"));
			results[query] = (unsigned char**) malloc(numDatasets * sizeof(unsigned char*));

			for (i = 0; i < numDatasets; i++){
				results[query][i] = NULL;
			}

			num_query++;

		}else if (strstr(line3, "Dataset") != NULL){
			char *p = strtok(line3, " \t\n");
			dataset = atoi(strtok(NULL, " \t\n"));
			results[query][dataset] =
				(unsigned char*) malloc(numGenes * sizeof(unsigned char));

			for (i = 0; i < numGenes; i++){
				results[query][dataset][i] = 255;  //255 is like NULL value
				num_entries++;
			}

			char *line4 = (char*) malloc(lineLen * sizeof(char));
			ret = fgets(line4, lineLen, infile);

			int gene;
			float score;

			p = strtok(line4, " :\t\n");
			gene = atoi(p);
			p = strtok(NULL, " :\t\n");
			//score = sqrt(atof(p));
			//score = log(atof(p));
			score = atof(p);

			//QIAN ADDED MAY 7
			if (score > 12.0){
				score = 12.0;
			}else if (score < -12.0){
				score = -12.0;
			}

			score += 12.0;

			results[query][dataset][gene] = (unsigned char) (score * 10.0);

			while (1){
				p = strtok(NULL, " :\t\n");

				if (p == NULL){
					break;
				}

				gene = atoi(p);
				p = strtok(NULL, " :\t\n");
				//score = sqrt(atof(p));
				//score = log(atof(p));
				score = atof(p);

				//QIAN ADDED MAY 7
				if (score > 12.0){
					score = 12.0;
				}else if (score < -12.0){
					score = -12.0;
				}

				score += 12.0;

				results[query][dataset][gene] = (unsigned char) (score * 10.0);
			}

			free(line4);
		}

		free(line2);
		free(line3);
	}

	free(line);

	unsigned char *** conv_results =
		(unsigned char***) malloc(numDatasets * sizeof(unsigned char**));
	conv_results[0] =
		(unsigned char **) malloc(numDatasets * numGenes * sizeof(unsigned char*));

	for (j = 1; j < numDatasets; j++){
		conv_results[j] = conv_results[j - 1] + numGenes;
	}

	for (j = 0; j < numDatasets; j++){
		for (i = 0; i < numGenes; i++){
			conv_results[j][i] = NULL;
		}
	}

	unsigned char *us = NULL;

	for (i = 0; i < numGenes; i++){
		if (results[i] == NULL){
			continue;
		}

		for (j = 0; j < numDatasets; j++){
			if (results[i][j] == NULL){
				continue;
			}

			if (us == NULL){
				conv_results[j][i] =
					(unsigned char*) malloc(num_entries * sizeof(unsigned char));
				us = conv_results[j][i];
			}else {
				conv_results[j][i] = us + numGenes;
				us = conv_results[j][i];
			}

			for (k = 0; k < numGenes; k++){
				conv_results[j][i][k] = results[i][j][k];
			}
		}
	}

	for (i = 0; i < numGenes; i++){
		if (results[i] == NULL){
			continue;
		}

		for (j = 0; j < numDatasets; j++){
			if (results[i][j] == NULL){
				continue;
			}

			free(results[i][j]);
		}

		free(results[i]);
	}

	free(results);
	fclose(infile);

	return conv_results;
}

float** read_dataset_linkage_graph(char *file){
	FILE *infile;
	char *line, *ret, *line2;
	int len;
	int i, j, k;

	if((infile=fopen(file, "r"))==NULL){
		printf("file, Not found \n");
		return NULL;
	}
	
    int lineLen = MAX_CHAR_PER_LINE;
    line = (char*) malloc(lineLen);
	line2 = (char*)malloc(lineLen);
	int start=0;
	char *p;

	FILE *infile2;
	char filename[500];

	float **r = (float**)malloc(numDatasets*sizeof(float*));
	r[0] = (float*)malloc(numDatasets*numDatasets*sizeof(float));

	for(i=1; i<numDatasets; i++){
		r[i] = r[i-1] + numDatasets;
	}
	for(i=0; i<numDatasets; i++){
		for(j=0; j<numDatasets; j++){
			r[i][j] = 0;
		}
	}

	unsigned short s1, s2;
	float deg;
    while (fgets(line, lineLen, infile) != NULL){
		p = strtok(line, " \n");
		s1 = atoi(p);
		p = strtok(NULL, " \n");
		s2 = atoi(p);
		p = strtok(NULL, " \n");
		deg = atof(p);
		//r[s1][s2] = deg;
		r[s2][s1] = deg;
	}

	fclose(infile);
	free(line);
	free(line2);
	return r;
}

