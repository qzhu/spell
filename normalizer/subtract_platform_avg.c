#include "subtract_platform_avg.h"

void subtract_platform_avg(
	unsigned char***r, struct Presence **present_g, struct Map *map_D,
	struct PlatformAvg *pavg, struct StrIntMap *dset_tree, struct Platforms *P)
{
	int i, ii, j, jj, k, kk;
	int d, dd;
	int g, gg;

	//omp_set_num_threads(4);
	int numThreads = omp_get_max_threads();

	//printf("Begin\n");
	int numDsetLoad = map_D->num_set;

	#pragma omp parallel for \
		shared(map_D, present_g, r, pavg, dset_tree, P) \
		private(dd, d, i, ii, g, gg) \
		firstprivate(numDsetLoad) \
		schedule(static)
	for(dd=0; dd<numDsetLoad; dd++){
		d = get_Map_r(dd, map_D);
		struct Presence *p = present_g[dd];
		struct Map *map_G = init_Map_from_Presence(p);
		int numG = map_G->num_set;

		//printf("dset %d of %d\n", dd, numDsetLoad);

		char *dset_name = get_StrIntMap_str(d, dset_tree);
		char *plat_name = strstr(dset_name, "GPL");
		int plat_id = get_StrIntMap_int(plat_name, P->m);
		free(dset_name);

		for(gg=0; gg<numG; gg++){
			g = get_Map_r(gg, map_G);
			if(r[d][g]==NULL) continue;

			float platform_mean = get_PlatformAvg_mean(g, pavg+plat_id);
			float platform_stdev = get_PlatformAvg_stdev(g, pavg+plat_id);
			for(ii=0; ii<numG; ii++){
				i = get_Map_r(ii, map_G);
				if(r[d][g][i]==255) continue;
				float rf = (float)r[d][g][i] / 10.0 - 12.0;
				rf = (rf - platform_mean) / platform_stdev;
				//rf = rf - platform_mean;
				rf = (rf + 12.0) * 10.0;
				if(rf<=1.0) rf = 1.0;
				if(rf>=254.0) rf = 254.0;
				r[d][g][i] = (unsigned char) rf;
			}
		}
		free_Map(map_G);
	}
	//printf("End\n");
}
