#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif

#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

#ifndef PLATFORM_AVG
#define PLATFORM_AVG
#include "../reader/platform_avg_reader.h"
#endif

void subtract_platform_avg(
	unsigned char***r, struct Presence **present_g, struct Map *map_D,
	struct PlatformAvg *pavg, struct StrIntMap *dset_tree, struct Platforms *P);

