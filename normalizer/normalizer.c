#include "normalizer.h"

int bresult_cmp_ascend(const void *a, const void *b){
    const struct bresult *c1 = (const struct bresult *) a;
    const struct bresult *c2 = (const struct bresult *) b;

    if (c1->f < c2->f){
        return -1;
    }else if (c1->f == c2->f){
        return 0;
    }else {
        return 1;
    }
}

int bresult_cmp_descend(const void *a, const void *b){
    const struct bresult *c1 = (const struct bresult *) a;
    const struct bresult *c2 = (const struct bresult *) b;

    if (c1->f > c2->f){
        return -1;
    }else if (c1->f == c2->f){
        return 0;
    }else {
        return 1; 
    }
}



float *** init_float3D(unsigned char ***r){
	int dd, d, i, j;

	float ***f = (float***)malloc(numDatasets*sizeof(float**));
	f[0] = (float**)malloc(numDatasets*numGenes*sizeof(float*));
	for(i=1; i<numDatasets; i++){
		f[i] = f[i-1] + numGenes;
	}

	for(i=0; i<numDatasets; i++){
		for(j=0; j<numGenes; j++){
			f[i][j] = NULL;
		}
	}

	int countLen = 0;
	for(d=0; d<numDatasets; d++){
		for(i=0; i<numGenes; i++){
			if(r[d][i]==NULL) continue;
			countLen+=numGenes;
		}
	}

	float *last = NULL;
	for(d=0; d<numDatasets; d++){
		for(i=0; i<numGenes; i++){
			if(r[d][i]==NULL) continue;
			if(last == NULL){
				f[d][i] = (float*)malloc(countLen*sizeof(float));
				last = f[d][i];
			}else{
				f[d][i] = last + numGenes;
				last = f[d][i];
			}
			for(j=0; j<numGenes; j++){
				f[d][i][j] = 0;
			}
		}
	}

	return f;
}


float *** rank_transform(unsigned char ***r){
	int d, dd, i, j;
	float ***f = init_float3D(r);
	int k;

	for(i=0; i<numGenes; i++){
		/*if(i%1000==0){
			printf("Normalizing %d of %d\n", i, numGenes);
		}*/
		int tot = 0;
        for(d=0; d<numDatasets; d++){
            if(r[d][i]==NULL) continue;
            tot+=numGenes;
        }
        if(tot==0){
            continue;
        }

        struct bresult *a = (struct bresult*)malloc(tot*sizeof(struct bresult));

        k = 0;
        for(d=0; d<numDatasets; d++){
            if(r[d][i]==NULL) continue;
            for(j=0; j<numGenes; j++){
                a[k].i = d;
				a[k].j = j;
				if(r[d][i][j]==255){
					a[k].f = -9999;
				}else{
                	a[k].f = (float) r[d][i][j];
				}
                k++;
            }
        }

        qsort(a, tot, sizeof(struct bresult), bresult_cmp_ascend);
        for(j=0; j<tot; j++){
            int id1 = a[j].i;
			int id2 = a[j].j;
            int p = tot + 1;
            if(a[j].f==-9999){
				f[id1][i][id2] = -9999;
            }else{
            	f[id1][i][id2] = ((float) (j+1) - (float) p / 2.0) / ((float) p/2.0);
            }
        }
        free(a);
    }

	
	/*for(i=0; i<numGenes; i++){
		for(d=0; d<numDatasets; d++){
			if(r[d][i]==NULL) continue;
			break;
		}
	}

	free(r[d][i]);
	free(r[0]);
	free(r);*/
	
	return f;
}
