#include "rank_transform.h"

void rank_transform_data(unsigned char***r, struct Presence **present_g, struct Map *map_D){
	int i, ii, j, jj, k, kk;
	int d, dd;
	int g, gg;

	int numDsetLoad = map_D->num_set;
	for(dd=0; dd<numDsetLoad; dd++){
		d = get_Map_r(dd, map_D);
		struct Presence *p = present_g[dd];
		struct Map *map_G = init_Map_from_Presence(p);
		int numG = map_G->num_set;

		for(gg=0; gg<numG; gg++){
			g = get_Map_r(gg, map_G);
			if(r[d][g]==NULL) continue;
			struct aresult *a =
				(struct aresult*)malloc(numG*sizeof(struct aresult));
			for(ii=0; ii<numG; ii++){
				i = get_Map_r(ii, map_G);
				a[ii].i = i;
				if(r[d][g][i]==255){
					a[ii].f = 0;
				}else{
					a[ii].f = (float) r[d][g][i];
				}
			}
			qsort(a, numG, sizeof(struct aresult), aresult_cmp);

			int bin_size = numG / 250;
			int b = 250;
			for(i=0; i<numG; i++){
				if(b!=0 && i%bin_size==0){
					b--;
				}
				if(r[d][g][a[i].i]==255){
					continue;
				}
				//printf("%d becomes %d\n", (int) r[d][g][a[i].i], b);
				r[d][g][a[i].i] = (unsigned char) b;
			}
			free(a);
		}
		free_Map(map_G);
	}
}

float inverse_erf(float x){
	float sign = 1.0;
	float PI = 3.1415926;
	float A = 0.140012;

	if(x<0){
		sign = -1.0;
	}
	float p0 = log(1.0-x*x);
	float p1 = p0/2.0;
	float p2 = (2.0/PI/A+p1);

	float pr1 = sqrt(sqrt(p2*p2 - p0/A) - (2.0/PI/A+p1));
	return sign*pr1;
}

float get_z_score_from_normal_distr(float x){
	return inverse_erf(2.0*x-1.0) * sqrt(2.0);
}

void rank_norm_transform_data(
	unsigned char***r,
	struct Presence **present_g, struct Map *map_D)
{
	int i, ii, j, jj, k, kk;
	int d, dd;
	int g, gg;

	int numDsetLoad = map_D->num_set;
	for(dd=0; dd<numDsetLoad; dd++){
		d = get_Map_r(dd, map_D);
		struct Presence *p = present_g[dd];
		struct Map *map_G = init_Map_from_Presence(p);
		int numG = map_G->num_set;

		for(gg=0; gg<numG; gg++){
			g = get_Map_r(gg, map_G);
			if(r[d][g]==NULL) continue;
			struct aresult *a =
				(struct aresult*)malloc(numG*sizeof(struct aresult));
			for(ii=0; ii<numG; ii++){
				i = get_Map_r(ii, map_G);
				a[ii].i = i;
				if(r[d][g][i]==255){
					a[ii].f = 0;
				}else{
					a[ii].f = (float) r[d][g][i];
				}
			}
			qsort(a, numG, sizeof(struct aresult), aresult_cmp);
			for(i=0; i<numG; i++){
				float percentile = (float) (numG - i) / (numG+1.0);
				float z = get_z_score_from_normal_distr(percentile);
				if(z>5.0){
					z = 5.0;
				}
				if(z<-5.0){
					z = -5.0;
				}
				if(r[d][g][a[i].i]==255){
					continue;
				}
				//printf("%d becomes %.2f\n", (int) r[d][g][a[i].i], z);
				r[d][g][a[i].i] = (unsigned char) ((z + 12.0) * 10.0);
			}
			free(a);
		}
		free_Map(map_G);
	}
}

