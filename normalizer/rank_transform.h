#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif

#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

void rank_transform_data(unsigned char***r, struct Presence **present_g, struct Map *map_D);
float inverse_erf(float x);
float get_z_score_from_normal_distr(float x);
void rank_norm_transform_data(
	unsigned char***r,
	struct Presence **present_g, struct Map *map_D);
