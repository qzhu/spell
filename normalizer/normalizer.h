#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif

#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

struct bresult{
    int i;
    int j;
    float f;
    float f2; //optional
};

int bresult_cmp_ascend(const void *a, const void *b);
int bresult_cmp_descend(const void *a, const void *b);
float *** init_float3D(unsigned char ***r);
float ***rank_transform(unsigned char ***r);

