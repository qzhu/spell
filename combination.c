#include "combination.h"

void order_statistics_integrate(
	int l, float **res_cross, int *qq, struct Map *map_D, int query_size)
{
	int i, j, k;
	char *is_query = (char*)malloc(numGenes);
	float *final_rank = (float*)malloc(numGenes*sizeof(float));

	int numDsetLoad = map_D->num_set;
	float **ranks = (float**)malloc(numDsetLoad*sizeof(float*));
	ranks[0] = (float*)malloc(numDsetLoad*numGenes*sizeof(float));
	for(i=1; i<numDsetLoad; i++){
		ranks[i] = ranks[i-1] + numGenes;
	}
	for(i=0; i<query_size; i++){
		is_query[qq[i]] = 1;
	}
	for(i=0; i<numDsetLoad; i++){
		for(j=0; j<numGenes; j++){
			ranks[i][j] = -9999;
		}
	}
	for(i=0; i<numDsetLoad; i++){
		int numGenesD = 0;
		for(j=0; j<numGenes; j++){
			if(res_cross[i][j]==0) continue;
			numGenesD++;
		}
		struct aresult *a = (struct aresult*)malloc(numGenesD*sizeof(struct aresult));
		k = 0;
		for(j=0; j<numGenes; j++){
			if(res_cross[i][j]==0) continue;
			a[k].i = j;
			a[k].f = res_cross[i][j];
			k++;
		}
		qsort(a, numGenesD, sizeof(struct aresult), aresult_cmp);
		for(j=0; j<numGenesD; j++){
			int g = a[j].i;
			ranks[i][g] = (float) (j+1) / (float) numGenesD;
		}
		free(a);
	}
	for(j=0; j<numGenes; j++){
		int numNonZero = 0;
		for(i=0; i<numDsetLoad; i++){
			if(ranks[i][j]==-9999) continue;
			numNonZero++;
		}
		if(numNonZero==0){
			final_rank[j] = -9999;
			continue;
		}
		gsl_vector_float *gs = gsl_vector_float_calloc(numNonZero);
		k=0;
		for(i=0; i<numDsetLoad; i++){
			if(ranks[i][j]==-9999) continue;
			gsl_vector_float_set(gs, k, ranks[i][j]);
			k++;
		}
		gsl_permutation * perm = gsl_permutation_alloc(numNonZero);
		gsl_permutation * rnk = gsl_permutation_alloc(numNonZero);

		gsl_sort_vector_float_index(perm, gs);
		gsl_permutation_inverse(rnk, perm);
		k=0;
		float max = -9999;
		for(i=0; i<numDsetLoad; i++){
			if(ranks[i][j]==-9999) continue;
			float p = gsl_vector_float_get(gs, k);
			unsigned int rk = rnk->data[k];
			double gg = gsl_cdf_binomial_Q(rk+1, p, numNonZero);
			float tmp = -1.0*log(gg);
			if(isinf(tmp)==1){
				//printf("is inf: %d %d %.2f %.15e\n", rk+1, numNonZero, p, gg);
				tmp = -9999;
			}
			if(tmp>max){
				max = tmp;
			}
			k++;
		}
		final_rank[j] = max;
		gsl_permutation_free(perm);
		gsl_permutation_free(rnk);
		gsl_vector_float_free(gs);
	}

	int numNonZero = 0;
	for(i=0; i<numGenes; i++){
		if(final_rank[i]==-9999) continue;
		if(is_query[i]==1) continue;
		numNonZero++;
	}

	struct aresult *b =
		(struct aresult*)malloc(numNonZero*sizeof(struct aresult));
	k = 0;
	for(i=0; i<numGenes; i++){
		if(final_rank[i]==-9999) continue;
		if(is_query[i]==1) continue;
		b[k].i = i;
		b[k].f = final_rank[i];
		k++;
	}

	qsort(b, numNonZero, sizeof(struct aresult), aresult_cmp);
	printf("Query %d\n", l);
	printf("Results:\n");
	for(i=0; i<numNonZero && i<500; i++){
		printf("%d %.5f\n", b[i].i, b[i].f);
	}
	free(b);

	for(i=0; i<query_size; i++){
		is_query[qq[i]] = 0;
	}

	free(is_query);
	free(final_rank);
	free(ranks[0]);	
	free(ranks);
}
