/*
 * common.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */

#include "common.h"

int *init_int(int size, int def){
	int *w = (int*)malloc(size*sizeof(int));
	int i = 0;
	for(i=0; i<size; i++){
		w[i] = def;
	}
	return w;
}

int **init_int_2d(int size1, int size2, int def){
	int **w = (int**)malloc(size1*sizeof(int*));
	w[0] = (int*)malloc(size1*size2*sizeof(int));
	int i = 0;
	int j = 0;
	for(i=1; i<size1; i++){
		w[i] = w[i-1] + size2;
	}
	for(i=0; i<size1; i++){
		for(j=0; j<size2; j++){
			w[i][j] = def;
		}
	}
	return w;
}

int ***init_int_3d(int size1, int size2, int size3, int def){
	int ***res_sing = (int***)malloc(size1*sizeof(int**));
	res_sing[0] = (int**)malloc(size1*size2*sizeof(int*));
	int l, i, j;
	for(l=1; l<size1; l++){
		res_sing[l] = res_sing[l-1] + size2;
	}
	res_sing[0][0] = (int*)malloc(size1*size2*size3*sizeof(int));
	for(l=0; l<size1; l++){
		for(i=0; i<size2; i++){
			if(l==0 && i==0){
			}else if(i==0){
				res_sing[l][i] = res_sing[l-1][size2-1] + size3;
			}else{
				res_sing[l][i] = res_sing[l][i-1] + size3;
			}
			for(j=0; j<size3; j++){
				res_sing[l][i][j] = def;
			}
		}
	}
	return res_sing;
}

void clear_int_2d(int **w, int size1, int size2){
	int i = 0;
	int j = 0;
	for(i=0; i<size1; i++){
		for(j=0; j<size2; j++){
			w[i][j] = 0;
		}
	}
}

void clear_int(int *w, int size){
	int i = 0;
	for(i=0; i<size; i++){
		w[i] = 0;
	}
}

void free_int_2d(int **w){
	free(w[0]);
	free(w);
}

void free_int_3d(int ***w){
	free(w[0][0]);
	free(w[0]);
	free(w);
}

float *init_float(int size, float def){
	float *w = (float*)malloc(size*sizeof(float));
	int i = 0;
	for(i=0; i<size; i++){
		w[i] = def;
	}
	return w;
}

float **init_float_2d(int size1, int size2, float def){
	float **w = (float**)malloc(size1*sizeof(float*));
	w[0] = (float*)malloc(size1*size2*sizeof(float));
	int i = 0;
	int j = 0;
	for(i=1; i<size1; i++){
		w[i] = w[i-1] + size2;
	}
	for(i=0; i<size1; i++){
		for(j=0; j<size2; j++){
			w[i][j] = def;
		}
	}
	return w;
}

float ***init_float_3d(int size1, int size2, int size3, float def){
	float ***res_sing = (float***)malloc(size1*sizeof(float**));
	res_sing[0] = (float**)malloc(size1*size2*sizeof(float*));
	int l, i, j;
	for(l=1; l<size1; l++){
		res_sing[l] = res_sing[l-1] + size2;
	}
	res_sing[0][0] = (float*)malloc(size1*size2*size3*sizeof(float));
	for(l=0; l<size1; l++){
		for(i=0; i<size2; i++){
			if(l==0 && i==0){
			}else if(i==0){
				res_sing[l][i] = res_sing[l-1][size2-1] + size3;
			}else{
				res_sing[l][i] = res_sing[l][i-1] + size3;
			}
			for(j=0; j<size3; j++){
				res_sing[l][i][j] = def;
			}
		}
	}
	return res_sing;
}

void clear_float_2d(float **w, int size1, int size2){
	int i = 0;
	int j = 0;
	for(i=0; i<size1; i++){
		for(j=0; j<size2; j++){
			w[i][j] = 0;
		}
	}
}

void clear_float(float *w, int size){
	int i = 0;
	for(i=0; i<size; i++){
		w[i] = 0;
	}
}

void free_float_2d(float **w){
	free(w[0]);
	free(w);
}

void free_float_3d(float ***w){
	free(w[0][0]);
	free(w[0]);
	free(w);
}


struct Float2D* init_Float2D_x(int d1){
	struct Float2D *fd = (struct Float2D*)malloc(sizeof(struct Float2D));
	fd->f = (float**)malloc(d1*sizeof(float*));
	int i = 0;
	for(i=0; i<d1; i++){
		fd->f[i] = NULL;
	}
	fd->is_matrix = 0;
	fd->is_compacted = 0;
	fd->tot = 0;
	fd->d1 = d1;
	fd->d2_size = (int*)malloc(d1*sizeof(int));
	for(i=0; i<d1; i++){
		fd->d2_size[i] = 0;
	}

	return fd;
}

struct Float2D* init_Float2D_y(int at_x, int d2, float def_val, struct Float2D *fd){
	#ifdef DEBUG
	if(fd==NULL){
		fprintf(stderr, "Error: Float2D is null in init_Float2D_y\n");
		exit(1);
	}
	#endif
	fd->f[at_x] = (float*)malloc(d2*sizeof(float));
	int i = 0;
	for(i=0; i<d2; i++){
		fd->f[at_x][i] = def_val;
	}
	fd->tot+=d2;
	fd->d2_size[at_x] = d2;
	fd->is_matrix = 0;
	fd->is_compacted = 0;
	return fd;
}

struct Float2D * init_Float2D(int d1, int d2, float def_val){
	struct Float2D *fd = (struct Float2D*)malloc(sizeof(struct Float2D));
	fd->f = (float**)malloc(d1*sizeof(float*));
	fd->f[0] = (float*)malloc(d1*d2*sizeof(float));
	int i, j;
	#ifdef DEBUG
	if(d1<=0 || d2<=0){
		fprintf(stderr, "Error: Float2D invalid dimension.\n");
		exit(1);
	}
	#endif
	for(i=1; i<d1; i++){
		fd->f[i] = fd->f[i-1]+d2;
	}
	for(i=0; i<d1; i++){
		for(j=0; j<d2; j++){
			fd->f[i][j] = def_val;
		}
	}
	fd->d1 = d1;
	fd->d2 = d2;
	fd->tot=d1*d2;
	fd->is_matrix = 1;
	fd->d2_size = NULL;
	return fd;
}

float get_Float2D_xy(int x, int y, const struct Float2D *fd){
	#ifdef DEBUG
	int i = -1;
	if(fd->is_matrix==0){
		i = fd->d2_size[x];
	}else{
		i = fd->d2;
	}
	if(!(x<fd->d1 && y<i)){
		fprintf(stderr, "BAD, index out of range!\n");
		exit(1);
	}
	#endif
	return fd->f[x][y];
}

float* get_Float2D_x(int x, const struct Float2D *fd){
	#ifdef DEBUG
	if(!x<fd->d1){
		fprintf(stderr, "BAD, index out of range! P1\n");
		exit(1);
	}
	#endif
	return fd->f[x];
}

void set_Float2D_xy(int x, int y, float v, struct Float2D *fd){
	#ifdef DEBUG
	int i = -1;
	if(fd->is_matrix==0){
		i = fd->d2_size[x];
	}else{
		i = fd->d2;
	}
	if(!(x<fd->d1 && y<i)){
		fprintf(stderr, "BAD, index out of range! P2\n");
		exit(1);
	}
	#endif
	fd->f[x][y] = v;
}

void increment_Float2D_xy(int x, int y, float v, struct Float2D *fd){
	#ifdef DEBUG
	int i = -1;
	if(fd->is_matrix==0){
		i = fd->d2_size[x];
	}else{
		i = fd->d2;
	}
	if(!(x<fd->d1 && y<i)){
		fprintf(stderr, "BAD, index out of range! P2\n");
		exit(1);
	}
	#endif
	fd->f[x][y] += v;
}

struct Float2D* compact_Float2D(struct Float2D *fd){
	#ifdef DEBUG
	if(fd->is_matrix==1 || fd->is_compacted == 1){
		fprintf(stderr, "No need to compact. return\n");
		return fd;
	}
	#endif

	float **tmp = (float**)malloc(fd->d1*sizeof(float*));
	int i, j;
	for(j=0; j<fd->d1; j++){
		tmp[j] = NULL;
	}

	char isFirst = 1;
	float *prev = NULL;
	int prev_id = 0;
	for(i=0; i<fd->d1; i++){
		if(fd->f[i]==NULL) continue;
		if(isFirst==1){
			isFirst = 0;
			tmp[i] = (float*)malloc(fd->tot*sizeof(float));
			prev = tmp[i];
			prev_id = i;
		}else{
			tmp[i] = prev + fd->d2_size[prev_id];
			prev = tmp[i];
			prev_id = i;
		}
	}

	for(i=0; i<fd->d1; i++){
		if(fd->f[i]==NULL) continue;
		for(j=0; j<fd->d2_size[i]; j++){
			tmp[i][j] = fd->f[i][j];
		}
	}

	for(i=0; i<fd->d1; i++){
		if(fd->f[i]==NULL) continue;
		free(fd->f[i]);
	}
	free(fd->f);

	fd->f = tmp;
	fd->is_compacted = 1;

	return fd;
}

void free_Float2D(struct Float2D *fd){
	if(fd->is_matrix==1){
		free(fd->f[0]);
		free(fd->f);
		free(fd);
	}else if(fd->is_compacted==1){
		int i = -1;
		for(i=0; i<fd->d1; i++){
			if(fd->f[i]==NULL) continue;
			break;
		}
		if(i!=-1 && i<fd->d1){
			free(fd->f[i]);
			free(fd->f);
			free(fd->d2_size);
			free(fd);
		}
	}else{
		int i = -1;
		for(i=0; i<fd->d1; i++){
			if(fd->f[i]==NULL) continue;
			free(fd->f[i]);
		}
		free(fd->f);
		free(fd->d2_size);
		free(fd);
	}
}


FILE * FOpen(char *filename, const char * mode){
	FILE *fp;
	if((fp=fopen(filename, mode))==NULL){
		fprintf(stderr, "Error: file %s does not exist.\n", filename);
		exit(1);
	}
	return fp;
}

struct Map* init_Map_2d(int size, int size2){
	struct Map* mm = (struct Map*) malloc(size2*sizeof(struct Map));
	int i = 0;
	for(i=0; i<size2; i++){
		struct Map* m = mm + i;
		m->size = size;
		m->f = (int*)malloc(size*sizeof(int));
		m->r = (int*)malloc(size*sizeof(int));
		int i = 0;
		for(i=0; i<size; i++){
			m->f[i] = -1;
			m->r[i] = -1;
		}
		m->num_set = 0;
	}
	return mm;
}

struct Map* init_Map(int size){
	struct Map* m = (struct Map*) malloc(sizeof(struct Map));
	m->size = size;
	m->f = (int*)malloc(size*sizeof(int));
	m->r = (int*)malloc(size*sizeof(int));
	int i = 0;
	for(i=0; i<size; i++){
		m->f[i] = -1;
		m->r[i] = -1;
	}
	m->num_set = 0;
	return m;
}

struct Map* init_Map_from_Presence(const struct Presence *p){
	struct Map* m = (struct Map*) malloc(sizeof(struct Map));
	m->size = p->num_bits;
	m->f = (int*)malloc(m->size*sizeof(int));
	m->r = (int*)malloc(m->size*sizeof(int));
	set_Map_from_Presence(p, m);
	return m;
}

struct Map* init_Map_from_Presence_reverse(const struct Presence *p){
	struct Map* m = (struct Map*) malloc(sizeof(struct Map));
	m->size = p->num_bits;
	m->f = (int*)malloc(m->size*sizeof(int));
	m->r = (int*)malloc(m->size*sizeof(int));
	set_Map_from_Presence_reverse(p, m);
	return m;
}

void set_Map_from_Presence(const struct Presence *p, struct Map *m){
	int i = 0;
	for(i=0; i<m->size; i++){
		m->f[i] = -1;
		m->r[i] = -1;
	}
	int j = 0;
	for(i=0; i<p->num_bits; i++){
		if(check_bit(i, p)==1){
			m->f[i] = j;
			m->r[j] = i;
			j++;
		}
	}
	m->num_set = j;
}

void set_Map_from_Presence_reverse(const struct Presence *p, struct Map *m){
	int i = 0;
	for(i=0; i<m->size; i++){
		m->f[i] = -1;
		m->r[i] = -1;
	}
	int j = 0;
	for(i=0; i<p->num_bits; i++){
		if(check_bit(i, p)==0){
			m->f[i] = j;
			m->r[j] = i;
			j++;
		}
	}
	m->num_set = j;
}

void add_to_Map(int g, struct Map *m){
	#ifdef DEBUG
	if(g >= m->size){
		fprintf(stderr, "out of bounds, add_to_map\n");
		exit(1);
	}
	#endif
	int j = m->num_set;
	m->f[g] = j;
	m->r[j] = g;
	m->num_set++;
}

void free_Map(struct Map *m){
	free(m->f);
	free(m->r);
	free(m);
}

void free_Map_2d(struct Map *m, int size){
	int i = 0;
	for(i=0; i<size; i++){
		free(m[i].f);
		free(m[i].r);
	}
	free(m);
}

void clear_Map(struct Map *m){
	int i = 0;
	for(i=0; i<m->size; i++){
		m->f[i] = -1;
		m->r[i] = -1;
	}
	m->num_set = 0;
}

int get_Map_f(int i, const struct Map *m){
	#ifdef DEBUG
	if(i>=m->size){
		fprintf(stderr, "error: get_Map_f out of bounds.\n");
		exit(1);
	}
	#endif
	return m->f[i];
}

int get_Map_r(int i, const struct Map *m){
	#ifdef DEBUG
	if(i>=m->size){
		fprintf(stderr, "error: get_Map_r out of bounds.\n");
		exit(1);
	}
	#endif
	return m->r[i];
}

struct StrIntMap *init_StrIntMap(){
	struct StrIntMap *strint = (struct StrIntMap*)malloc(sizeof(struct StrIntMap));
	rb_red_blk_tree *tree = RBTreeCreate(CharArrayComp,
		CharArrayDest, IntDest, CharArrayPrint, InfoPrint);
	rb_red_blk_tree *tree_r = RBTreeCreate(IntComp,
		IntDest, CharArrayDest, IntPrint, InfoPrint);
	strint->size = 0;
	strint->f = tree;
	strint->r = tree_r;
	strint->str_size = 100;
	return strint;
}

struct StrIntMap *read_StrIntMap(char *map_file){
	struct StrIntMap *strint = init_StrIntMap();
	FILE *ft = fopen(map_file, "r");

	char *line, *ret;
	int len;
	int lineLen = MAX_CHAR_PER_LINE;
	line = (char*)malloc(lineLen);

	while (fgets(line, lineLen, ft) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(ft, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, ft);
		}
	}

	rewind(ft);
	int numLines = 0;
	int size = 0;
	int str_len = 100;
	strint->str_size = str_len;

	while (fgets(line, lineLen, ft) != NULL){
		char *pp = (char*)malloc(str_len);
		char *p = strtok(line, " \t\n");
		sprintf(pp, "%s", p);
		int *id = (int*)malloc(sizeof(int));
		*id = atoi(strtok(NULL, " \t\n"));
		RBTreeInsert(strint->f, pp, id);
		RBTreeInsert(strint->r, id, pp);
		numLines++;
		strint->size++;
	}

	fclose(ft);
	free(line);

    return strint;
}

void set_StrIntMap(char *name, int id, struct StrIntMap *tree){
#ifdef DEBUG
	if(strlen(name)>=tree->str_size){
		fprintf(stderr, "str_size not enough, too small\n");
		exit(1);
	}
#endif
	char *nn = (char*)malloc(tree->str_size);
	sprintf(nn, "%s", name);
	int *iid = (int*)malloc(sizeof(int));
	*iid = id;
	RBTreeInsert(tree->f, nn, iid);
	RBTreeInsert(tree->r, iid, nn);
	tree->size++;
}

int get_StrIntMap_int(char *name, struct StrIntMap *tree){
	rb_red_blk_node *node1 = RBExactQuery(tree->f, name);
	int i = -1;
	if(node1==0){
		fprintf(stderr, "Error, str_name %s is not found", name);
		getchar();
		exit(1);
	}else{
		int *ip = (int*)node1->info;
		i = *ip;
	}
	return i;
}

char* get_StrIntMap_str(int id, struct StrIntMap *tree){
	int *iid = (int*)malloc(sizeof(int));
	*iid = id;
	rb_red_blk_node *node1 = RBExactQuery(tree->r, iid);
	free(iid);
	char *p = NULL;
	if(node1==0){
		fprintf(stderr, "Error, id_name %d is not found", id);
		getchar();
		exit(1);
	}else{
		p = (char*)malloc(tree->str_size);
		sprintf(p, "%s", (char*)node1->info);
	}
	return p;
}

char** get_StrIntMap_all_keys_str(struct StrIntMap *tree){
	stk_stack* st = RBGetAllNodes(tree->f);

	rb_red_blk_node *newNode;
	char **strs = (char**)malloc(tree->size*sizeof(char*));
	strs[0] = (char*)malloc(tree->size*tree->str_size);
	int i = 0;
	for(i=1; i<tree->size; i++){
		strs[i] = strs[i-1] + tree->str_size;
	}
	i = 0;
	while ( (newNode = StackPop(st)) ) {
		sprintf(strs[i], "%s", (char*)newNode->key);
		i++;
	}
	return strs;
}

int* get_StrIntMap_all_keys_int(struct StrIntMap *tree){
	stk_stack* st = RBGetAllNodes(tree->r);
	rb_red_blk_node *newNode;
	int *is = (int*)malloc(tree->size*sizeof(int));
	int i = 0;
	while ( (newNode = StackPop(st)) ) {
		int *ip = (int*) newNode->key;
		is[i] = *ip;
		i++;
	}
	return is;
}

void free_StrIntMap(struct StrIntMap *tree){
	RBTreeDestroy(tree->f);
	RBTreeDestroy(tree->r);
	free(tree);
}

struct Platform *init_Platform(){
	struct Platform *p = (struct Platform*)malloc(sizeof(struct Platform));
	return p;
}

struct StrIntMap *merge_Platform_mapping(struct Platforms *ps){
	int i, j;
	struct StrIntMap *strint = init_StrIntMap();

	for(i=0; i<ps->numPlatforms; i++){
		struct Platform *p = ps->platform+i;
		char **ss = get_StrIntMap_all_keys_str(p->m);
		for(j=0; j<p->m->size; j++){
			char *new_name = (char*)malloc(p->m->str_size);
			sprintf(new_name, "%s_%s", ss[j], p->platform_name);
			set_StrIntMap(new_name, strint->size, strint);
			//printf("Map %s %d\n", new_name, strint->size);
			free(new_name);
		}
		free(ss[0]);
		free(ss);
	}
	return strint;
}


