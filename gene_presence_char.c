#include "gene_presence.h"

/*
 * gene_presence.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */

struct Presence * init_Presence_2d(int num_bits, int size){
	struct Presence *c = (struct Presence *) malloc(size*sizeof(struct Presence));
	int i = 0;
	for(i=0; i<size; i++){
		struct Presence *cc = c + i;
		cc->p = (char*)malloc(num_bits);
		cc->size_char = num_bits;
		cc->num_bits = num_bits;
		int j = 0;
		for(j=0; j<cc->size_char; j++){
			cc->p[j] = 0;
		}
	}
	return c;
}

void free_Presence_2d(struct Presence *p, int size){
	int i;
	for(i=0; i<size; i++){
		free(p[i].p);
	}
	free(p);
}

struct Presence * init_Presence_from_char(char *c, int num_bits){
	struct Presence *d = init_Presence(num_bits);
	int j = 0;
	for(j=0; j<num_bits; j++){
		if(c[j]==1){
			set_bit(j, d);
		}
	}
	return d;
}


struct Presence * init_Presence(int num_bits){
	struct Presence *c = (struct Presence *) malloc(sizeof(struct Presence));
	c->p = (char*)malloc(num_bits);
	c->size_char = num_bits;
	c->num_bits = num_bits;
	int j = 0;
	for(j=0; j<c->size_char; j++){
		c->p[j] = 0;
	}
	return c;
}

void free_Presence(struct Presence *c){
	free(c->p);
	free(c);
}

void clear_Presence(struct Presence *c){
	int i;
	for(i=0; i<c->size_char; i++){
		c->p[i] = 0;
	}
}

struct Presence * copy_Presence(struct Presence *p){
	struct Presence *c = (struct Presence *) malloc(sizeof(struct Presence));
	c->p = (char*)malloc(p->size_char);
	c->size_char = p->size_char;
	c->num_bits = p->num_bits;
	int j = 0;
	for(j=0; j<c->size_char; j++){
		c->p[j] = p->p[j];
	}
	return c;
}

//check_present
int check_bit(int g, const struct Presence *c){
	#ifdef DEBUG
	if(c==NULL){
		fprintf(stderr, "Error!!!\n");
		exit(1);
	}
	if(g >= c->num_bits){
		fprintf(stderr, "Index out of range, gene presence\n");
		exit(1);
	}
	#endif
	return c->p[g];
}

void set_bit(int g, struct Presence *c){
	#ifdef DEBUG
	if(c==NULL){
		fprintf(stderr, "Error!!!\n");
		exit(1);
	}
	if(g >= c->num_bits){
		fprintf(stderr, "Index out of range, gene presence\n");
		exit(1);
	}
	#endif
	c->p[g] = 1;
}

void clear_bit(int g, struct Presence *c){
	#ifdef DEBUG
	if(c==NULL){
		fprintf(stderr, "Error!!!\n");
		exit(1);
	}
	if(g >= c->num_bits){
		fprintf(stderr, "Index out of range, gene presence\n");
		exit(1);
	}
	#endif
	c->p[g] = 0;
}
