
#include "adder_file_read.h"

#ifndef GENE_PRESENCE
#define GENE_PRESENCE
#include "gene_presence.h"
#endif

#ifndef COMMON
#define COMMON
#include "common.h"
#endif

#include "reader/avg_file_reader.h"
#ifndef SINFO
#define SINFO
#include "reader/sinfo_file_reader.h"
#endif

#ifndef PLATFORM_AVG
#define PLATFORM_AVG
#include "reader/platform_avg_reader.h"
#endif

#include "reader/var_file_reader.h"
#include "reader/settings_reader.h"
#include "reader/gene_data_reader.h"

#include "adder_one_query.h"
#include "combination.h"
#include "dataset_adder/linear_combination.h"
#include "gene_adder/sequential.h"
#include "weighter/cross_validated.h"
#include "evaluator/evaluator.h"
#include "displayer/normal_output.h"
#include "normalizer/rank_transform.h"
#include "normalizer/subtract_platform_avg.h"
#include "reader/query_reader.h"
#include "time.h"

void do_block_query(
	int start_index, int end_index, struct Queries *qu, 
	struct Presence **present_g, unsigned char ***r, 
	struct Presence *q_to_load, gsl_rng *rnd, int numDsetLoad, 
	struct Map *map_D, struct StrIntMap *dset_tree,
	struct sinfo *si, struct Float2D *avg, struct Float2D *var 
	)
{
	int numGenesD = 0;
	//omp_set_num_threads(4);
	int numThreads = omp_get_max_threads();

	int **cr_q;
	int i, j, k, l;
	int d, dd, qi, qj, ii;
	//printf("GOOD %d", numThreads); getchar();
	//important stuff begins =====================================
	//clock_t start = clock();
	//printf("Before integration"); getchar();
	float RATE = 0.95;
	int FOLD = 5;
	enum PartitionMode PART_M = CUSTOM_PART;

	for(l=start_index; l<=end_index; l++){

		int query_size = qu->query_size[l];
		int *qq = qu->q[l];

		int fold = FOLD;
		if(query_size < fold){
			fold = query_size;
		}
		int fold_size = query_size / fold;
		if(query_size % fold > 0){
			fold_size++;
		}

		struct Presence *query = init_Presence(numGenes);
		for(i=0; i<query_size; i++){
			if(check_bit(qq[i], q_to_load)==1){
				set_bit(qq[i], query);
			}
		}
		struct Map *map_this_q = init_Map_from_Presence(query);

		#if defined (WEIGHTED)
		cr_q = partition_query(&fold, &fold_size, qq, query_size, rnd, PART_M);
		#endif

		//universal
		float *resl = init_float(numGenes, 0);
		float *sum_wl = init_float(numGenes, 0);
		float *wl = init_float(numDsetLoad, 0);
		int *q_size = init_int(numDsetLoad, 0);
		int *countsl = init_int(numGenes, 0);

		//for saving fold precision only (WEIGHTED)
		float **wql = NULL;
		float **sum_wql = NULL;
		float **resql = NULL;

		int **countsdl = NULL;

		//printf("Query %d of %d\n", l, numQueries);
		#if defined (WEIGHTED)
		//for sum_wql, countsql, resql
		float ***res_omp = init_float_3d(numThreads, fold, numGenes, 0);
		float ***sum_w_omp = init_float_3d(numThreads, fold, numGenes, 0);
		int ***counts_omp = NULL;
		wql = init_float_2d(numDsetLoad, fold, 0);
		sum_wql = init_float_2d(fold, numGenes, 0);
		resql = init_float_2d(fold, numGenes, 0);

		//for normal
		countsdl = init_int_2d(numDsetLoad, numGenes, 0);
		#elif defined (EQUAL_WEIGHTED) || defined (ORDER_STATISTICS) || defined (VARIANCE_WEIGHTING)
		float **res_omp = init_float_2d(numThreads, numGenes, 0);
		float **sum_w_omp = init_float_2d(numThreads, numGenes, 0);
		int **counts_omp = init_int_2d(numThreads, numGenes, 0);
		#endif

		float **res_crossl = NULL;
		float **res_singl = NULL;
		float ***res_sing_omp = NULL;

		#if defined (ORDER_STATISTICS) || defined (WEIGHTED)
		res_crossl = init_float_2d(numDsetLoad, numGenes, 0); //unweighted gene score (by dataset)
		#endif

		#if defined (METACOR_SING)
		res_singl = init_float_2d(query_size, numGenes, 0);
		res_sing_omp = init_float_3d(numThreads, query_size, numGenes, 0);
		#endif

		struct Presence *absent_G_omp = init_Presence_2d(numGenes, numThreads);
		struct Presence *absent_Q_omp = init_Presence_2d(numGenes, numThreads);
		struct Presence *is_query_omp = init_Presence_2d(numGenes, numThreads);
		struct Presence *is_query_cross_omp = init_Presence_2d(numGenes, numThreads);
		struct Presence *is_gold_omp = init_Presence_2d(numGenes, numThreads);
		struct Map *map_G_omp = init_Map_2d(numGenes, numThreads);

		#pragma omp parallel for \
			shared(map_D, present_g, map_this_q, qq, r, wl, wql, q_size, sum_w_omp, \
				counts_omp, countsdl, res_omp, res_crossl, res_sing_omp, \
				absent_G_omp, absent_Q_omp, is_query_omp, is_query_cross_omp, \
				is_gold_omp, map_G_omp, cr_q) \
			private(dd, d, qi, i, ii, j, k) \
			firstprivate(fold, fold_size, numDsetLoad, query_size, l, numGenesD, numGenes) \
			schedule(static)
		for(dd=0; dd<numDsetLoad; dd++){
			d = get_Map_r(dd, map_D);
			int tid = omp_get_thread_num();

			#if defined (WEIGHTED)
			//for normal
			float *w = wql[dd];
			int *countsd = countsdl[dd];

			//for per query gene
			float **res = res_omp[tid];
			float **sum_w = sum_w_omp[tid];
			#elif defined (EQUAL_WEIGHTED) || defined (ORDER_STATISTICS) || defined (VARIANCE_WEIGHTING)
			float *w = wl + dd;
			float *res = res_omp[tid];
			float *sum_w = sum_w_omp[tid];
			int *counts = counts_omp[tid];
			#endif

			int *iq_size = q_size + dd;

			#if defined (ORDER_STATISTICS) || defined (WEIGHTED)
			float *res_cross = res_crossl[dd];
			#endif
			#if defined (METACOR_SING)
			float **res_sing = res_sing_omp[tid];
			#endif

			struct Presence *absent_G = absent_G_omp + tid;
			struct Presence *absent_Q = absent_Q_omp + tid;
			struct Presence *is_query = is_query_omp + tid;
			struct Presence *is_query_cross = is_query_cross_omp + tid;
			struct Presence *is_gold = is_gold_omp + tid;
			struct Map *map_G = map_G_omp + tid;

			//printf("Thread %d %d H", tid, dd); getchar();
			float **f = NULL;
			//changed: absent_G, absent_Q, map_G
			#if defined (METACOR_SING)
			f = prepare_one_query_in_one_dataset_metacor(absent_G, absent_Q, map_G,
				present_g[dd], map_this_q, r[d], avg, si, d);
			#elif defined (NO_SUBAVGZ)
			f = prepare_one_query_in_one_dataset_no_subavgz(absent_G, absent_Q, map_G,
				present_g[dd], map_this_q, r[d], avg, d);
			#else
			f = prepare_one_query_in_one_dataset_normal(absent_G, absent_Q, map_G,
				present_g[dd], map_this_q, r[d]);
			#endif

			numGenesD = map_G->num_set;

			//calculating q_size============================
			*iq_size = 0;
			//SET
			for(qi=0; qi<query_size; qi++){
				int tq = qq[qi];
				if(check_bit(tq, absent_Q)==1) continue;
				set_bit(tq, is_query);
				//*iq_size = (*iq_size) + 1;
			}

			for(qi=0; qi<fold; qi++){
				int nn = 0;
				for(qj=0; qj<fold_size; qj++){
					if(cr_q[qi][qj]==-1) continue;
					nn++;
				}
				if(nn==0) continue;
				*iq_size = (*iq_size) + 1;
			}

			if(*iq_size == 0){
				free_float_2d(f);
				continue;
			}
			//============================================

			float *rank_normal = seq(f, absent_G, is_query, map_this_q, map_G);

			//RESTORE
			for(qi=0; qi<query_size; qi++){
				int tq = qq[qi];
				if(check_bit(tq, absent_Q)==1) continue;
				clear_bit(tq, is_query);
			}

			//WEIGHTING ==================================================================
			#if defined (WEIGHTED)
			float **rank_cross = init_float_2d(fold, numGenes, 0);
			float *ww = weight_one_dataset_cross_validated(rank_cross,
				qq, query_size, absent_Q, absent_G, f, map_G,
				map_this_q, is_query_cross, is_gold, fold, cr_q, fold_size, RATE);
			memcpy(w, ww, sizeof(float)*fold);
			free(ww);
			//for per query gene summing
			for(qi=0; qi<fold; qi++){
				for(i=0; i<numGenes; i++){
					res[qi][i] += rank_cross[qi][i] * w[qi];
				}
			}
			free_float_2d(rank_cross);
			for(qi=0; qi<fold; qi++){
				for(ii=0; ii<numGenesD; ii++){
					i = get_Map_r(ii, map_G);
					sum_w[qi][i] += w[qi];
				}
			}
			//for normal summing
			for(ii=0; ii<numGenesD; ii++){
				i = get_Map_r(ii, map_G);
				countsd[i]++;
			}
			#elif defined (METACOR_SING)
			metacor_sing_one_dataset(qq, query_size, absent_Q, absent_G, f,
				map_G, map_this_q, is_query_cross, is_gold, res_sing);

			#elif defined (EQUAL_WEIGHTED) || defined (ORDER_STATISTICS)
			*w = 0.001;

			#elif defined (VARIANCE_WEIGHTING)
			for(qi=0; qi<query_size; qi++){
				int this_q, next_q;	
				this_q = qq[qi];
				if(check_bit(this_q, absent_Q)==1) continue;
				*w = (*w) + get_Float2D_xy(d, this_q, var);
			}
			*w = *w / (float) (*iq_size);
			#endif

			//=======================================================================END
			#if defined (EQUAL_WEIGHTED) || defined (ORDER_STATISTICS) || defined (VARIANCE_WEIGHTING)
			for(ii=0; ii<numGenesD; ii++){
				i = get_Map_r(ii, map_G);
				sum_w[i] += (*w);
				counts[i]++;
			}
			for(i=0; i<numGenes; i++){
				res[i] += rank_normal[i] * (*w);
			}
			#endif
			//===============================================================END
			#if defined (ORDER_STATISTICS) || defined (WEIGHTED)
			for(i=0; i<numGenes; i++){
				res_cross[i] = rank_normal[i];
			}
			#endif
			free_float_2d(f);
			free(rank_normal);
		}

		#if defined (WEIGHTED)
		/*int TOP = 50;
		struct aresult *ar = (struct aresult*)malloc(numDsetLoad*sizeof(struct aresult));
		for(i=0; i<numDsetLoad; i++){
			ar[i].i = i;
			ar[i].f = wl[i];
		}
		qsort(ar, numDsetLoad, sizeof(struct aresult), aresult_cmp);
		for(i=TOP; i<numDsetLoad; i++){
			wl[ar[i].i] = 0;
		}
		free(ar);*/
		for(j=0; j<numDsetLoad; j++){
			for(i=0; i<numGenes; i++){
				if(countsdl[j][i]==1){
					countsl[i]++;
				}
			}
		}
		for(j=0; j<numDsetLoad; j++){
			for(qi=0; qi<fold; qi++){
				wl[j] += wql[j][qi];
			}
			if(q_size[j]>0){
				wl[j] /= q_size[j];
			}
		}

		//for per query gene summing
		/*for(i=0; i<numThreads; i++){
			for(qi=0; qi<fold; qi++){
				for(j=0; j<numGenes; j++){
					sum_wql[qi][j]+=sum_w_omp[i][qi][j];
					resql[qi][j]+=res_omp[i][qi][j];
				}
			}
		}	
		for(qi=0; qi<fold; qi++){	
			for(j=0; j<numGenes; j++){
				if(countsl[j]<=0.50*numDsetLoad){
					resql[qi][j] = 0;
					continue;
				}
				if(sum_wql[qi][j]>0){
					resql[qi][j]/=sum_wql[qi][j];
				}
			}
		}
		float *qw = (float*)malloc(fold*sizeof(float));
		for(qi=0; qi<fold; qi++){
			struct Presence *query_q = init_Presence(numGenes);
			for(i=0; i<fold_size; i++){
				if(cr_q[qi][i]==-1) continue;
				set_bit(cr_q[qi][i], query_q);
			}
			struct Presence *gold_q = init_Presence(numGenes);
			for(qj=0; qj<fold; qj++){
				if(qi==qj) continue;
				for(i=0; i<fold_size; i++){
					if(cr_q[qj][i]==-1) continue;
					if(check_bit(cr_q[qj][i], query_q)==1) continue;
					set_bit(cr_q[qj][i], gold_q);
				}
			}
			struct Map *map_Gq = init_Map(numGenes);
			for(i=0; i<numGenes; i++){
				if(resql[qi][i]==0) continue;
				add_to_Map(i, map_Gq);
			}
			struct eval_result *ee = evaluate(resql[qi], query_q, gold_q, map_Gq, 0.99);
			printf("Cross val %d: %.5f\n", qi, ee->rbp);
			qw[qi] = ee->rbp;
			free(ee->ret_genes);
			free(ee->pr);
			free(ee);
			free_Map(map_Gq);
			free_Presence(query_q);
			free_Presence(gold_q);
		}*/
		/*
		for(i=0; i<numGenes; i++){
			sum_wl[i] = 0;
			for(qi=0; qi<fold; qi++){
				resl[i] += resql[qi][i] * qw[qi];
				sum_wl[i]+=qw[qi];
			}
			if(sum_wl[i]>0){
				resl[i] /= sum_wl[i];
			}
		}*/
		//free(qw);		

		//for normal summing =======================
		for(j=0; j<numDsetLoad; j++){
			for(i=0; i<numGenes; i++){
				if(countsdl[j][i]==1){
					sum_wl[i] += wl[j];
				}
			}
		}
		for(i=0; i<numGenes; i++){
			for(j=0; j<numDsetLoad; j++){
				resl[i] += res_crossl[j][i] * wl[j];
			}
			if(sum_wl[i]>0){
				resl[i]/=sum_wl[i];
			}
		}
		
		free(cr_q[0]);
		free(cr_q);

		#elif defined (EQUAL_WEIGHTED) || defined (ORDER_STATISTICS) || defined (VARIANCE_WEIGHTING)
		//Merge step
		for(i=0; i<numThreads; i++){
			for(j=0; j<numGenes; j++){
				sum_wl[j]+=sum_w_omp[i][j];
				resl[j]+=res_omp[i][j];
				countsl[j]+=counts_omp[i][j];
			}
			#if defined (METACOR_SING)
			for(k=0; k<query_size; k++){
				for(j=0; j<numGenes; j++){
					res_singl[k][j]+=res_sing_omp[i][k][j];
				}
			}
			#endif
		}
		//divide by sum
		for(i=0; i<numGenes; i++){
			if(sum_wl[i]>0){
				resl[i]/=sum_wl[i];
			}
		}
		#endif

		#if defined (METACOR_SING)
		struct Map *map_g = init_Map(numGenes);
		for(i=0; i<numGenes; i++){
			map_g->r[i] = i;
			map_g->f[i] = i;
		}
		map_g->num_set = i;
		numGenesD = numGenes;

		struct Presence *is_query = init_Presence(numGenes);
		for(qi=0; qi<query_size; qi++){
			set_bit(qq[qi], is_query);
		}
		printf("Query %d\n", l);
		for(qi=0; qi<query_size; qi++){
			struct eval_result *er = evaluate(res_singl[qi], is_query, Gold, map_g, 0.95);
			printf("Precision: %.5f\n", er->rbp);
			free(er->pr);
			free(er->ret_genes);
			free(er);
		}
		free_Presence(is_query);
		free_float_3d(res_sing_omp);
		free_float_2d(res_singl);
		free_Map(map_g);
		#else
		  #if defined (ORDER_STATISTICS)
		order_statistics_integrate(l, res_crossl, qq, map_D, query_size);
	  	  #else
		output_results(l, wl, sum_wl, countsl, resl, qq, query_size, map_D, dset_tree);
		  #endif
		  #if defined (ORDER_STATISTICS) || defined (WEIGHTED)
			free_float_2d(res_crossl);
		  #endif
		#endif

		free(q_size);
		free(wl);
		free(countsl);
		free(sum_wl);
		free(resl);
		free_Presence(query);
		free_Map(map_this_q);
		free_Presence_2d(is_query_omp, numThreads);
		free_Presence_2d(is_query_cross_omp, numThreads);
		free_Presence_2d(is_gold_omp, numThreads);
		free_Presence_2d(absent_Q_omp, numThreads);
		free_Presence_2d(absent_G_omp, numThreads);
		free_Map_2d(map_G_omp, numThreads);

		#if defined (WEIGHTED)
		free_int_2d(countsdl);
		free_float_2d(wql);
		free_float_3d(sum_w_omp);
		free_float_3d(res_omp);
		free_float_2d(sum_wql);
		free_float_2d(resql);
		#elif defined (EQUAL_WEIGHTED) || defined (ORDER_STATISTICS) || defined (VARIANCE_WEIGHTING)
		free_float_2d(sum_w_omp);
		free_int_2d(counts_omp);
		free_float_2d(res_omp);
		#endif

	}

}

int main(int argc, char *argv[]){

	#if defined (METACOR_SING)
	if(argc!=5){
		printf("query_file dset_list go_term_file go_term\n");
		return 0;
	}
	#else
	if(argc!=3){
		printf("query_file dset_list\n");
		return 0;
	}
	#endif

	const gsl_rng_type *T;
	gsl_rng *rnd;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	rnd = gsl_rng_alloc(T);

	omp_set_num_threads(8);

	//reading map=================================================
	char *map_file = (char*)malloc(100);
	sprintf(map_file, "%s", "adder_config.txt");
	//printf("before reading adder_config.txt"); getchar();
	struct Platforms *P = read_Platforms(map_file);
	//printf("done, before executing merge"); getchar();
	struct StrIntMap *dset_tree = merge_Platform_mapping(P);
	numDatasets = dset_tree->size;
	numGenes = 26000; //probably need to make this general
	free(map_file);

	//printf("done reading adder_config.txt");

	int i,j,k,d,l;
	int qi, qj;
	int ii, jj, kk;

	struct Queries *qu = query_reader(argv[1]);	

	FILE *infile_l;
	FILE *ifs;
	char *line = (char*)malloc(1024);
	char *filename = (char*)malloc(1024);
	char c[4];
	char *p;
	int ret = 0;

	struct Float2D *avg = NULL;
	struct Float2D *var = NULL;
	struct goldstd *gs = NULL;
	char *gold = NULL;
	struct Presence *Gold = NULL;

	//=========================================================END
	//argv[2] stores the list of datasets to load
	#if defined (METACOR_SING) || defined (NO_SUBAVGZ)
	avg = init_Float2D_x(dset_tree->size);
	avg = read_avgfile(argv[2], P->avg_path, dset_tree, avg);
	avg = compact_Float2D(avg);
	#endif

	#if defined (VARIANCE_WEIGHTING)
	var = init_Float2D_x(dset_tree->size);
	var = read_varfile(argv[2], P->var_path, dset_tree, var);
	var = compact_Float2D(var);
	#endif

	#if defined (METACOR_SING)
	gs = gold_standard_read(argv[3]);
	int gs_id = find_gold_std_id(argv[4], gs);
	if(gs_id==-1){
		fprintf(stderr, "Error: term not found\n");
		exit(1);
	}
	gold = gs->gold[gs_id];
	Gold = init_Presence_from_char(gold, numGenes);
	#endif

	//printf("before reading sinfo"); getchar();
	struct sinfo* si = NULL;
	for(i=0; i<P->numPlatforms; i++){
		char *new_name = (char*)malloc(100);
		sprintf(new_name, "%s/%s", P->sinfo_path, P->platform[i].platform_name);
		si = read_sinfo(new_name, si, P->platform+i, dset_tree);
		free(new_name);
	}

	//printf("after reading sinfo"); getchar();
	struct PlatformAvg *pavg = (struct PlatformAvg *)
			malloc(P->numPlatforms*sizeof(struct PlatformAvg));
	for(i=0; i<P->numPlatforms; i++){
		char *new_name = (char*)malloc(100);
		sprintf(new_name, "%s/%s", P->platform_avg_path, P->platform[i].platform_name);
		read_platform_avg(new_name, numGenes, pavg+i);
		free(new_name);
	}
	//printf("after reading pavg"); getchar();

	struct Presence *d_to_load = init_Presence(numDatasets);
	FILE *inf = FOpen(argv[2], "r");
	while (fgets(line, 1024, inf)!=NULL){
		char *p = strtok(line, " \t\n");
		char *pp = (char*)malloc(100);
		char *s = (char*)malloc(100);
		sprintf(pp, "%s", p); //pp = gse
		p = strtok(NULL, " \t\n"); //p = platform
		sprintf(s, "%s_%s", pp, p); //s = "gse"_"platform"
		int dset = get_StrIntMap_int(s, dset_tree);
		set_bit(dset, d_to_load);
		free(pp);
		free(s);
	}
	fclose(inf);

	int dd;
	for(dd=0; dd<numDatasets; dd++){
		if(si[dd].mean==-9999 && si[dd].stdev==-9999){
			clear_bit(dd, d_to_load);
			continue;
		}
	}

	struct Map *map_D = init_Map_from_Presence(d_to_load);
	int numDsetLoad = map_D->num_set;


	struct Presence *q_to_load = init_Presence(numGenes);
	char isLast = 0;
	int start = 0;
	int num_list = 0;	

	for(l=0; l<qu->num_query; l++){
		struct Presence *b = init_Presence(numGenes);
		for(i=0; i<qu->query_size[l]; i++){
			if(check_bit(qu->q[l][i], q_to_load)==0){
				set_bit(qu->q[l][i], b);
			}
		}

		struct Map *mQ = init_Map_from_Presence(b);
		int nq = mQ->num_set;

		for(i=0; i<P->numPlatforms; i++){
			int numDsetInPlatform = P->platform[i].m->size;
			for(kk=0; kk<nq; kk++){
				k = get_Map_r(kk, mQ);
				sprintf(filename, "%s/%s/%d.bin", P->bin_path, P->platform[i].platform_name, k);
				if(access(filename, F_OK)==0){
					num_list+=numDsetInPlatform;
				}
			}
		}

		if(num_list>105000 || l==qu->num_query-1){
			isLast=1;
		}

		for(i=0; i<qu->query_size[l]; i++){
			if(check_bit(qu->q[l][i], q_to_load)==0){
				set_bit(qu->q[l][i], q_to_load);
			}
		}

		free_Map(mQ);
		free_Presence(b);

		if(isLast==0){
			continue;
		}

		//isLast==1
		struct Map *map_Q = init_Map_from_Presence(q_to_load);
		int num_unique_q = map_Q->num_set;
	
		//data reading===========================================
		float *us = NULL;
		int init_f;
		unsigned char ***rr = NULL;
		unsigned char ***r = NULL;

		//printf("before reading gene"); getchar();	
		struct GeneData *gd = init_GeneData(P->numPlatforms*num_unique_q);
		for(i=0; i<P->numPlatforms; i++){
			for(kk=0; kk<num_unique_q; kk++){
				k = get_Map_r(kk, map_Q);
				sprintf(filename, "%s/%s/%d.bin", P->bin_path, P->platform[i].platform_name, k);
				ifs = fopen(filename, "rb");
				if(ifs==NULL){
					fprintf(stderr, "filename %s not exists, skipped\n", filename); 
					continue;
				}
				rr = read_gene_data(ifs, rr, P->platform+i, dset_tree, gd);
				fclose(ifs);
			}
		}

		r = compact_gene_data(rr, gd);
		int ind1 = TMP1;
		int ind2 = TMP2;

		//func = fix_func_network_g(func_tmp); //func_tmp is freed
		//=======================================================END
		//present_g[d][g]
		struct Presence **present_g = (struct Presence**)malloc(numDsetLoad*sizeof(struct Presence*));
		for(i=0; i<numDsetLoad; i++){
			present_g[i] = init_Presence(numGenes);
		}

		dd = 0;
		//omp_set_num_threads(4);
		#pragma omp parallel for \
			shared(map_D, present_g, r) \
			private(dd, d, i, j) \
			firstprivate(numGenes, numDsetLoad) \
			schedule(static)
		for(dd=0; dd<numDsetLoad; dd++){
			d = get_Map_r(dd, map_D);
			for(i=0; i<numGenes; i++){
				if(r[d][i]==NULL){
					continue;
				}
				set_bit(i, present_g[dd]);
				for(j=0; j<numGenes; j++){
					if(r[d][i][j]!=255){
						set_bit(j, present_g[dd]);
					}
				}
			}
		}

		//rank_transform_data(r, present_g, map_D);
		//rank_norm_transform_data(r, present_g, map_D);
		subtract_platform_avg(r, present_g, map_D, pavg, dset_tree, P);

		fprintf(stderr, "Block: %d %d %d\n", start, l, num_unique_q);
		do_block_query(start, l, qu, present_g, r, q_to_load, rnd, numDsetLoad, 
			map_D, dset_tree, si, avg, var);

		for(i=0; i<numDsetLoad; i++){
			free_Presence(present_g[i]);
		}
		free(present_g);
		free(r[ind1][ind2]);
		free(r[0]);
		free(r);
		free_GeneData(gd);
		free_Map(map_Q);

		clear_Presence(q_to_load);
		start = l+1;
		isLast = 0;		
		num_list = 0;

	}

	gsl_rng_free(rnd);
	free_Presence(q_to_load);
	free_sinfo(si);
	free_PlatformAvg(pavg);
	free_Presence(d_to_load);
	free_Map(map_D);

	free(line);
	free(filename);

	#if defined (METACOR_SING) ||defined (NO_SUBAVGZ)
	free_Float2D(avg);
	#endif 

	#if defined (VARIANCE_WEIGHTING)
	free_Float2D(var);
	#endif

	#if defined (METACOR_SING)
	free_Presence(Gold);
	free_goldstd(gs);
	#endif

	return 0;
}
