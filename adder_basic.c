#include "adder_basic.h"

void CharArrayDest(void *a){
	free((char *)a);
}

int CharArrayComp(const void *a, const void *b){
	char *ca = (char*) a;
	char *cb = (char*) b;
	if(strcmp(ca, cb)<0){
		return -1;
	}else if(strcmp(ca, cb)>0){
		return 1;
	}else{
		return 0;
	}
}

void CharArrayPrint(const void* a){
	char* ca = (char*) a;
	printf("%s", ca);
}

//for red black tree
void IntDest(void* a){
	free((int *) a);
}
int IntComp(const void * a, const void* b){
	if ( *(int*) a > *(int*) b){
		return(1);
	}

	if ( *(int*) a < *(int*) b){
		return(-1);
	}

	return(0);
}
void IntPrint(const void* a){
	printf("%d", *(int*) a);
}

void InfoPrint(void *a){
	;
}

void InfoDest(void *a){
	;
}

void DoNotDestroyRBNode(void *a){
	;
}

//end

//check whether something is in the tree or not
/*
int checkInTree(rb_red_blk_tree* tree, int g1, int g2){
	rb_red_blk_node* node1 = RBExactQuery(tree, &g1);

	if (node1 == 0){
		return 0;
	}

	node1 = RBExactQuery((rb_red_blk_tree*) node1->info, &g2);

	if (node1 == 0){
		return 0;
	}else {
		//printf("Here\n"); getchar();
		return 1;
	}
}*/

int aresult_cmp(const void *a, const void *b){
	const struct aresult *c1 = (const struct aresult *) a;
	const struct aresult *c2 = (const struct aresult *) b;

	if (c1->f > c2->f){
		return -1;
	}else if (c1->f == c2->f){
		return 0;
	}else {
		return 1;
	}
}

message* addMessage(char *m, int m_size, message *list, char *size, int limit, int isString){
	char *x = (char*)malloc(m_size+1);
	int j;
	int pad = 0; 
	for(j=0; j<m_size; j++){
		if(isString==1){
			if(m[j]=='\0'){
				pad = 1;
			}
			if(pad==1){
				x[j] = '\0';
			}else{
				x[j] = m[j];
			}
		}else{
			x[j] = m[j];
		}
	}
	x[m_size] = '\0';

	message *i = (message*)malloc(sizeof(message));
	i->m = x;
	i->m_size = m_size+1;

	//printf("%d %d\n", *((int*)i->m), i->m_size); getchar();

	message *curr = list;

	if (curr == NULL){
		i->next = curr;
		list = i;
		*size = *size + 1;
		return list;
	}

	i->next = curr;
	list = i;
	*size = *size + 1;

	message *pr_curr = NULL;

	if(*size==limit+1){
		curr = list;
		while (1){
			if(curr->next==NULL){
				break;
			}
			pr_curr = curr;
			curr = curr->next;
		}
		free(curr->m);
		free(curr);
		pr_curr->next = NULL;
		*size = limit;
	}
	return list;
}

message* destructMessages(message *i, char *size){
	message *curr = i;
	message *n = NULL;

	while (curr != NULL){
		n = curr->next;
		free(curr->m);
		free(curr);
		curr = n;
	}

	curr = NULL;
	n = NULL;
	*size = 0;
	return curr;
}

item* addItem(item *i, item *list, char *size, int limit){
	item *curr = list;

	//int limit = 10;

	if (curr == NULL){
		i->next = curr;
		list = i;
		*size = *size + 1;
		return list;
	}

	if (*size == limit &&  i->val <= list->val){
		free(i);
		return list;

	}else if (*size < limit && i->val <= list->val){
		i->next = curr;
		list = i;
		*size = *size + 1;
		return list;
	}

	item *pr_curr = NULL;

	while (curr != NULL){
		if (curr->val <= i->val){
			pr_curr = curr;
			curr = curr->next;
		}else  {
			break;
		}
	}

	if (pr_curr != NULL){
		pr_curr->next = i;
		i->next = curr;
		*size = *size + 1;
	}

	if (*size == limit + 1){
		item *n = list->next;
		free(list);
		list = n;
		*size = *size - 1;
	}

	return list;

}


item* destructList(item *i, char* size){
	item *curr = i;
	item *n = NULL;

	while (curr != NULL){
		n = curr->next;
		free(curr);
		curr = n;
	}

	curr = NULL;
	n = NULL;
	*size = 0;
	return curr;
}

float sumList(item *i){
	item *curr = i;
	float s = 0;
	int num = 0;

	while (curr != NULL){
		s += curr->val;
		num++;
		curr = curr->next;
	}

	return (float) (s / num);
}

int containsItem(item *i, unsigned short dset){
	item *curr = i;
	char isFound = 0;
	while (curr!=NULL){
		if(curr->dset==dset){
			isFound = 1;
			break;
		}
		curr=curr->next;
	}
	return isFound;
}

void printList(item *i){
	item *curr = i;

	while (curr != NULL){
		printf("%.2f ", curr->val);
		curr = curr->next;
	}

	printf("\n");
}
