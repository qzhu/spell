/*
 * sinfo_file_reader.h
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 *
 */

#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif
#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

struct sinfo{
	float mean;
	float stdev;
};

void free_sinfo(struct sinfo *s);

struct sinfo* read_sinfo(
	char *filelist, struct sinfo* ds, struct Platform *plat,
	struct StrIntMap *global_map);
