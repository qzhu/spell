#include "platform_avg_reader.h"


void init_PlatformAvg(int num_g, struct PlatformAvg *avg){
	if(avg==NULL){
		fprintf(stderr, "Platform avg null\n");
		exit(1);
	}
	avg->num_g = num_g;
	avg->mean = (float*)malloc(num_g*sizeof(float));
	avg->stdev = (float*)malloc(num_g*sizeof(float));
	int i;
	for(i=0; i<num_g; i++){
		avg->mean[i] = -9999;
		avg->stdev[i] = -9999;
	}
}

void set_PlatformAvg(int g, float mean, float stdev, struct PlatformAvg *avg){
	#ifdef DEBUG
	if(g<0 || g>=avg->num_g){
		fprintf(stderr, "Error, out of bounds, set_PlatformAvg\n");
		exit(1);
	}
	#endif
	avg->mean[g] = mean;
	avg->stdev[g] = stdev;
}

float get_PlatformAvg_mean(int g, struct PlatformAvg *avg){
	#ifdef DEBUG
	if(g<0 || g>=avg->num_g){
		fprintf(stderr, "Error, out of bounds, set_PlatformAvg\n");
		exit(1);
	}
	if(avg->mean[g]==-9999){
		fprintf(stderr, "Error, mean for gene %d not initialized, get_PlatformAvg\n", g);
		exit(1);
	}	
	#endif
	return avg->mean[g];
}

float get_PlatformAvg_stdev(int g, struct PlatformAvg *avg){
	#ifdef DEBUG
	if(g<0 || g>=avg->num_g){
		fprintf(stderr, "Error, out of bounds, set_PlatformAvg\n");
		exit(1);
	}
	if(avg->stdev[g]==-9999){
		fprintf(stderr, "Error, stdev for gene %d not initialized, get_PlatformAvg\n", g);
		exit(1);
	}	
	#endif
	return avg->stdev[g];
}

void free_PlatformAvg(struct PlatformAvg *a){
	free(a->mean);
	free(a->stdev);
	free(a);
}

void read_platform_avg(char *file, int num_g, struct PlatformAvg *avg){
	if(avg==NULL){
		fprintf(stderr, "Platform avg null\n");
		exit(1);
	}
	init_PlatformAvg(num_g, avg);

	FILE *fi = FOpen(file, "r");
	char *line = (char*)malloc(1024);
	int lines = 0;
	while (fgets(line, 1024, fi)!=NULL){
		lines++;
	}
	rewind(fi);
	while (fgets(line, 1024, fi)!=NULL){
		char *p = strtok(line, " \t\n");
		int g = atoi(p);
		p = strtok(NULL,  " \t\n");
		float mean = atof(p);
		p = strtok(NULL,  " \t\n");
		float stdev = atof(p);
		set_PlatformAvg(g, mean, stdev, avg);
	}
	fclose(fi);
	free(line);
}

