#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif
#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

struct Float2D* read_avg(char *file, struct Float2D *us, int d);
struct Float2D* read_avgfile(
	char *filelist, char *dir, struct StrIntMap *global_map,
	struct Float2D *avg);
