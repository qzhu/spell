#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif
#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

struct Float2D* read_variance(char *filename, struct Float2D *sv, int d);
struct Float2D* read_varfile(
	char *filelist, char *dir, struct StrIntMap *global_map,
	struct Float2D *var);
