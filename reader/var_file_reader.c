/*
 * adder_var_file_reader.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */
#include "var_file_reader.h"

struct Float2D* read_variance(char *filename, struct Float2D *sv, int d){
	if(sv==NULL){
		fprintf(stderr, "Error, Float2D is null in read_variance!\n");
		exit(1);
	}

    char *line, *ret;
    int len, i, j;
    int lineLen = MAX_CHAR_PER_LINE;
	FILE *infile  = FOpen(filename, "r");

	if(sv->f[d]==NULL){
		sv = init_Float2D_y(d, numGenes, -9999, sv);
	}
   
	line = (char*) malloc(lineLen);
    while (fgets(line, lineLen, infile) != NULL){
        while (strlen(line) == lineLen - 1){
            len = strlen(line);
            fseek(infile, -len, SEEK_CUR);
            lineLen += MAX_CHAR_PER_LINE;
            line = (char*) realloc(line, lineLen);
            ret = fgets(line, lineLen, infile);
        }
    }
    rewind(infile);
    while (fgets(line, lineLen, infile)!=NULL){
        char *p = strtok(line, " \t\n");
        int ind = atoi(p);
        if(p==NULL) continue;
        float val = atof(strtok(NULL, " \t\n"));
		set_Float2D_xy(d, ind, val, sv);
    }
    free(line);
    fclose(infile);

	//normalize variance
	struct aresult* a = (struct aresult*)malloc(numGenes*sizeof(struct aresult));
	for(i=0; i<numGenes; i++){
		a[i].i = i;
		if(get_Float2D_xy(d, i, sv)==-9999){
			a[i].f = 0;
		}else{
			a[i].f = get_Float2D_xy(d, i, sv);
		}
	}
	qsort(a, numGenes, sizeof(struct aresult), aresult_cmp);
	for(i=0; i<numGenes; i++){
		if(i>1000){
			if(get_Float2D_xy(d, a[i].i, sv)==-9999) continue;
			set_Float2D_xy(d, a[i].i, 0, sv);
		}else{
			set_Float2D_xy(d, a[i].i, (1.0-0.99)*pow(0.99, i), sv);
		}

		//set_Float2D_xy(sv, d, a[i].i, numGenes - i);
	}
	free(a);

    return sv;
}

struct Float2D* read_varfile(
	char *filelist, char *dir, struct StrIntMap *global_map,
	struct Float2D *var)
{
	//struct Float2D *var = init_Float2D_x(global_map->size);

	FILE *fi = FOpen(filelist, "r");
	char *line = (char*)malloc(1024);
	int lines = 0;
	while (fgets(line, 1024, fi)!=NULL){
		lines++;
	}
	rewind(fi);
	char filename[1024];
	while (fgets(line, 1024, fi)!=NULL){
		char *p = strtok(line, " \t\n");
		char *pp = (char*)malloc(100); //pp = gse
		char *s = (char*)malloc(100);
		sprintf(pp, "%s", p);
		p = strtok(NULL, " \t\n"); //p = platform
		sprintf(s, "%s_%s", pp, p); //s = "gse"_"platform"
		sprintf(filename, "%s/%s/%s.var", dir, p, pp);
		var = read_variance(filename, var, get_StrIntMap_int(s, global_map));
		free(s);
		free(pp);
	}

	fclose(fi);
	free(line);
	return var;
}
