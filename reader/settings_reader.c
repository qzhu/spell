#include "settings_reader.h"

struct Platforms *read_Platforms(char *filename){
	FILE *fi = FOpen(filename, "r");
	char *line = (char*)malloc(1024);
	int lines = 0;
	int start = 0;
	int numPlatforms = 0;
	while (fgets(line, 1024, fi)!=NULL){
		char *p = strtok(line, " \t\n");
		//printf("line: %s\n", p); getchar();
		if(strstr(p, "Platforms:")!=NULL){
			start = 1;
		}else if(start==1){
			numPlatforms++;
		}
		lines++;
	}
	rewind(fi);

	//printf("Here %d", numPlatforms); getchar();

	struct Platforms *P = (struct Platforms*)malloc(sizeof(struct Platforms));
	P->platform = (struct Platform *)malloc(numPlatforms*sizeof(struct Platform));
	P->numPlatforms = numPlatforms;
	//var_path, avg_path currently not supported
	P->sinfo_path = (char*)malloc(200);
	P->platform_avg_path = (char*)malloc(200);
	P->dataset_conv_path = (char*)malloc(200);
	P->bin_path = (char*)malloc(200);
	P->m = init_StrIntMap();


	start = 0;
	int i = 0;
	while (fgets(line, 1024, fi)!=NULL){
		char *p = strtok(line, " \t\n");
		if(strstr(p, "by_gene-directory")!=NULL){
			sprintf(P->bin_path, "%s", strtok(NULL, " \t\n"));
			continue;
		}
		if(strstr(p, "sinfo-directory")!=NULL){
			sprintf(P->sinfo_path, "%s", strtok(NULL, " \t\n"));
			continue;
		}
		if(strstr(p, "gene-based-platform-average")!=NULL){
			sprintf(P->platform_avg_path, "%s", strtok(NULL, " \t\n"));
			continue;
		}
		if(strstr(p, "dataset-conversion")!=NULL){
			sprintf(P->dataset_conv_path, "%s", strtok(NULL, " \t\n"));
			continue;
		}
		if(strstr(p, "Platforms:")!=NULL){
			start = 1;
		}else if(start==1){
			P->platform[i].platform_name = (char*)malloc(20);
			sprintf(P->platform[i].platform_name, "%s", p);

			//printf("Platform %s", p); getchar();

			P->platform[i].platform_id = atoi(strtok(NULL, " \t\n"));
			set_StrIntMap(P->platform[i].platform_name, P->platform[i].platform_id, P->m);
			i++;
		}
	}
	free(line);
	fclose(fi);

	//printf("Here2"); getchar();

	for(i=0; i<numPlatforms; i++){
		char *fname = (char*)malloc(1024);
		sprintf(fname, "%s/%s", P->dataset_conv_path, P->platform[i].platform_name);
		//printf("path %s", fname); getchar();
		P->platform[i].m = read_StrIntMap(fname);
		free(fname);
	}

	//printf("Here3"); getchar();

	return P;
}
