#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif
#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

struct PlatformAvg{
	float *mean;
	float *stdev;
	int num_g;
};

void init_PlatformAvg(int num_g, struct PlatformAvg *avg);
void set_PlatformAvg(int g, float mean, float stdev, struct PlatformAvg *avg);
float get_PlatformAvg_mean(int g, struct PlatformAvg *avg);
float get_PlatformAvg_stdev(int g, struct PlatformAvg *avg);

void free_PlatformAvg(struct PlatformAvg *a);
void read_platform_avg(char *file, int num_g, struct PlatformAvg *avg);


