#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif
#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

struct Queries{
	int num_query;
	int **q;
	int *query_size;
};

struct Queries* query_reader(char *term);
