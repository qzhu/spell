#ifndef BASIC_STRUCT
#define BASIC_STRUCT
#include "../adder_basic.h"
#endif
#ifndef COMMON
#define COMMON
#include "../common.h"
#endif

struct GeneData{
	int *g;
	int *d;
	int capacity;
	int size;
};

void free_GeneData(struct GeneData *gd);
struct GeneData* init_GeneData(int size);
unsigned char*** read_gene_data(
	FILE *infile, unsigned char ***conv_g, struct Platform *platform,
	struct StrIntMap *global_map, struct GeneData *gd);

unsigned char*** compact_gene_data(unsigned char*** conv_g, struct GeneData *gd);
