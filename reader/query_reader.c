#include "query_reader.h"

struct Queries* query_reader(char *term){
	FILE *infile;
	char *line, *ret;
	int len;
	int i, j, k;
	char filename[500];
	sprintf(filename, "%s", term);

	int lineLen = MAX_CHAR_PER_LINE;

	if ((infile = fopen(filename, "r")) == NULL){
		fprintf(stderr, "Error no such file \n");
		return NULL;
	}

	line = (char*) malloc(lineLen);

	//============GLOBAL================
	numQueries = 0;
	//==================================

	while (fgets(line, lineLen, infile) != NULL){
		while (strlen(line) == lineLen - 1){
			len = strlen(line);
			fseek(infile, -len, SEEK_CUR);
			lineLen += MAX_CHAR_PER_LINE;
			line = (char*) realloc(line, lineLen);
			ret = fgets(line, lineLen, infile);
		}

		numQueries = numQueries + 1;
	}

	rewind(infile);

	struct Queries *qq = (struct Queries*)malloc(sizeof(struct Queries));
	qq->num_query = numQueries;
	qq->q = (int**)malloc(numQueries*sizeof(int*));
	qq->query_size = (int*)malloc(numQueries*sizeof(int));

	for(i=0; i<numQueries; i++){
		qq->q[i] = NULL;
		qq->query_size[i] = 0;
	}

	for (i = 0; i < numQueries; i++){
		ret = fgets(line, lineLen, infile);
		strtok(line, " \t\n");
		int s = 1;
		while(strtok(NULL, " \t\n")!=NULL){
			s++;
		}
		qq->q[i] = (int*)malloc(s*sizeof(int));
		qq->query_size[i] = s;
	}

	rewind(infile);

	for (i = 0; i < numQueries; i++){
		ret = fgets(line, lineLen, infile);
		char *p = strtok(line, " \t\n");
		qq->q[i][0] = atoi(p);
		for (j = 1; j < qq->query_size[i]; j++){
			p = strtok(NULL, " \t\n");
			qq->q[i][j] = atoi(p);
		}
	}

	fclose(infile);
	free(line);

	return qq;
}
