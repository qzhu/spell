/*
 * avg_file_reader.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */

#include "avg_file_reader.h"

struct Float2D* read_avg(char *file, struct Float2D *us, int d){
	if(us==NULL){
		fprintf(stderr, "Error, Float2D is null in read_avg!\n");
		exit(1);
	}

	FILE *f = FOpen(file, "rb");
	int *n = (int*)malloc(sizeof(int));
	float *ff = (float*)malloc(sizeof(float));
	int ret;
	ret = fread((char*) n, 1, 4, f);
	int num_dset = *n;
	ret = fread((char*) n, 1, 4, f);
	int num_gene = *n;
	ret = fread((char*) n, 1, 4, f);
	int dset = d;

	int sz1 = ftell(f);
	int i = 0;
	if(us->f[dset]==NULL){
		us = init_Float2D_y(dset, numGenes, -1, us);
	}

	fseek(f, 0L, SEEK_END);
	int sz = ftell(f);
	int num_entry = (sz - sz1) / 8;

	fseek(f, 0L, SEEK_SET);
	fseek(f, 12, SEEK_CUR);

	for(i=0; i<num_entry; i++){
		ret = fread((char*) n, 1, 4, f);
		int g = *n;
		ret = fread((char*) ff, 1, 4, f);
		float fl = *ff;
		set_Float2D_xy(dset, g, fl, us);
	}
	free(n);
	free(ff);

	fclose(f);
	return us;
}

struct Float2D* read_avgfile(
	char *filelist, char *dir, struct StrIntMap *global_map,
	struct Float2D *avg)
{
	//struct Float2D *avg = init_Float2D_x(global_map->size);

	int i;
	FILE *fi = FOpen(filelist, "r");
	char *line = (char*)malloc(1024);
	int lines = 0;
	while (fgets(line, 1024, fi)!=NULL){
		lines++;
	}
	rewind(fi);
	char filename[1024];
	while (fgets(line, 1024, fi)!=NULL){
		char *p = strtok(line, " \t\n");
		char *pp = (char*)malloc(100);
		char *s = (char*)malloc(100);
		sprintf(pp, "%s", p); //pp = gse
		p = strtok(NULL, " \t\n"); //p = platform
		sprintf(s, "%s_%s", pp, p); //s = "gse"_"platform"
		sprintf(filename, "%s/%s/%s.avg.bin", dir, p, pp);
		avg = read_avg(filename, avg, get_StrIntMap_int(s, global_map));
		free(pp);
		free(s);
	}

	//avg = compact_Float2D(avg);
	fclose(fi);
	free(line);
	return avg;
}
