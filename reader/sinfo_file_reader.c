/*
 * sinfo_file_reader.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */

#include "sinfo_file_reader.h"

void free_sinfo(struct sinfo *s){
	free(s);
}

struct sinfo* read_sinfo(
	char *filelist, struct sinfo* ds, struct Platform *plat,
	struct StrIntMap *global_map)
{
	int i;
	if(ds==NULL){
		ds = (struct sinfo*)malloc(global_map->size * sizeof(struct sinfo));
		for(i=0; i<global_map->size; i++){
			ds[i].mean = -9999;
			ds[i].stdev = -9999;
		}
	}
	FILE *fi = FOpen(filelist, "r");
	char *line = (char*)malloc(1024);
	int lines = 0;
	while (fgets(line, 1024, fi)!=NULL){
		lines++;
	}
	rewind(fi);
	while (fgets(line, 1024, fi)!=NULL){
		char *p = strtok(line, " \t\n");
		int dd = atoi(p);

		char *gse = get_StrIntMap_str(dd, plat->m);
		char *new_name = (char*)malloc(100);
		sprintf(new_name, "%s_%s", gse, plat->platform_name);
		int d = get_StrIntMap_int(new_name, global_map);
		free(gse);
		free(new_name);

		p = strtok(NULL,  " \t\n");
		float mean = atof(p);
		p = strtok(NULL,  " \t\n");
		float stdev = atof(p);
		ds[d].mean = mean;
		ds[d].stdev = stdev;
	}
	fclose(fi);
	free(line);
	return ds;
}

