/*
 * gene_map_reader.c
 *
 *  Created on: Apr 21, 2012
 *      Author: qzhu
 */
#include "gene_data_reader.h"

void free_GeneData(struct GeneData *gd){
	free(gd->d);
	free(gd->g);
	free(gd);
}

struct GeneData* init_GeneData(int size){
	struct GeneData *gd = (struct GeneData*)malloc(sizeof(struct GeneData));

	gd->d = (int*)malloc(size*sizeof(int));
	gd->g = (int*)malloc(size*sizeof(int));
	int i;
	for(i=0; i<size; i++){
		gd->d[i] = -1;
		gd->g[i] = -1;
	}
	gd->capacity = size;
	gd->size = 0;

	return gd;
}

unsigned char*** read_gene_data(
	FILE *infile, unsigned char ***conv_g, struct Platform *platform,
	struct StrIntMap *global_map, struct GeneData *gd)
{
	char *line, *ret;
	int len;
	int i, j, k, d, dd, g;
	char c[4];
	size_t st;

	st = fread(c, 1, 4, infile);
	int num_dset = *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_gene = *((int*)c);
	st = fread(c, 1, 4, infile);
	int num_entries = *((int*)c);
	int nd = global_map->size;
	int ng = numGenes;

	if(conv_g==NULL){
		conv_g = (unsigned char***)malloc(ng*sizeof(unsigned char**));
		conv_g[0] = (unsigned char**)malloc(nd*ng*sizeof(unsigned char*));
		for(i=1; i<ng; i++){
			conv_g[i] = conv_g[i-1] + nd;
		}
		for(i=0; i<ng; i++){
			for(j=0; j<nd; j++){
				conv_g[i][j] = NULL;
			}
		}
	}

	unsigned short *s;
	unsigned char *us = NULL;
	int num_list = num_entries / num_gene;
	//printf("HH numlist %d ng %d numGenes %d\n", num_list, num_gene, numGenes); 
	unsigned char *v = (unsigned char*)malloc(ng);
	for(i=0; i<num_list; i++){
		st = fread(c, 1, 4, infile);
		s = (unsigned short*) c;
		dd = *s;
		s++;
		g = *s;

		char *gse = get_StrIntMap_str(dd, platform->m);
		char *new_name = (char*)malloc(100);
		sprintf(new_name, "%s_%s", gse, platform->platform_name);

		//printf("new name %s", new_name); getchar();
		d = get_StrIntMap_int(new_name, global_map);
		//printf("corr. id %d\n", d);
		//printf("%d %d\n", d, g); 
		free(gse);
		free(new_name);

		if(us==NULL){
			if(gd->size==gd->capacity){
				fprintf(stderr, "GeneData reached capacity\n");
				exit(1);
			}
			gd->d[gd->size] = d;
			gd->g[gd->size] = g;
			gd->size++;
			//printf("%d", gd->size); getchar();
			conv_g[g][d] = (unsigned char*)malloc(nd * ng);
			//conv_g[g][d] = (unsigned char*)malloc(num_list * ng * sizeof(unsigned char));
			//printf("%d %d %d a %x\n", g, d, nd, conv_g[g][d]);
			//printf("numD numG: %d %d\n", nd, ng);
			us = conv_g[g][d];
		}else{
			conv_g[g][d] = us + ng;
			//printf("%d %d b %x\n", g, d, conv_g[g][d]);
			us = conv_g[g][d];
		}

		for(j=num_gene; j<ng; j++){
			conv_g[g][d][j] = 255;
		}
	
		st = fread((char*) v, 1, num_gene, infile);
		if(st!=num_gene){
			printf("Error\n"); getchar();
		}
		
		for(j=0; j<num_gene; j++){
			conv_g[g][d][j] = v[j];
		}
		//printf("After"); getchar();
		//printf("DD\n");
	}
	free(v);
	//printf("DX\n");

	return conv_g;
}

unsigned char*** compact_gene_data(unsigned char*** conv_g, struct GeneData *gd){
	int i, j, k;

	//printf("NumDsets %d %d", numDatasets, numGenes); getchar();

	unsigned char *** conv_results =
		(unsigned char***) malloc(numDatasets * sizeof(unsigned char**));

	//printf("Here 4"); getchar();

	conv_results[0] =
		(unsigned char **) malloc(numDatasets * numGenes * sizeof(unsigned char*));


	for (j = 1; j < numDatasets; j++){
		conv_results[j] = conv_results[j - 1] + numGenes;
	}
	
	//printf("Here 3"); getchar();

	for (j = 0; j < numDatasets; j++){
		for (i = 0; i < numGenes; i++){
			conv_results[j][i] = NULL;
		}
	}

	//printf("Here 2"); getchar();

	long long int num_entries = 0;
	int num_list = 0;

	for (i = 0; i < numGenes; i++){
		for (j = 0; j < numDatasets; j++){
			if(conv_g[i][j]!=NULL){
				num_list++;
			}
		}
	}

	num_entries = (long long int) num_list * (long long int) numGenes;

	//printf("Here"); getchar();
	unsigned char *us = NULL;

	for (i = 0; i < numGenes; i++){

		for(j=0; j<numDatasets; j++){
			if (conv_g[i][j] != NULL){
				break;
			}
		}
		if(j==numDatasets){
			continue;
		}

		for (j = 0; j < numDatasets; j++){
			if (conv_g[i][j] == NULL){
				continue;
			}

			if (us == NULL){
				TMP1 = j;
				TMP2 = i;
				conv_results[j][i] =
					(unsigned char*) malloc(num_entries * sizeof(unsigned char));
				us = conv_results[j][i];
			}else {
				conv_results[j][i] = us + numGenes;
				us = conv_results[j][i];
			}

			for (k = 0; k < numGenes; k++){
				conv_results[j][i][k] = conv_g[i][j][k];
			}
		}
	}

	for(i=0; i<gd->size; i++){
		free(conv_g[gd->g[i]][gd->d[i]]);
	}

	//free(conv_g[gd->g][gd->d]);
	free(conv_g[0]);
	free(conv_g);

	/*
	for (i = 0; i < numGenes; i++){
		for(j=0; j<numDatasets; j++){
			if (conv_g[i][j] != NULL){
				free(conv_g[i][j]);
				break;
			}
		}
	}
	free(conv_g[0]);
	free(conv_g);
	*/
	return conv_results;
}
